<?php
/**
 * @file
 * psb_user_role_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function psb_user_role_permissions_user_default_roles() {
  $roles = array();

  // Exported role: Academic & Career Content Editor.
  $roles['Academic & Career Content Editor'] = array(
    'name' => 'Academic & Career Content Editor',
    'weight' => '11',
    'machine_name' => '',
  );

  // Exported role: Administrators Content Editor.
  $roles['Administrators Content Editor'] = array(
    'name' => 'Administrators Content Editor',
    'weight' => '22',
    'machine_name' => '',
  );

  // Exported role: Admissions Content Editor.
  $roles['Admissions Content Editor'] = array(
    'name' => 'Admissions Content Editor',
    'weight' => '26',
    'machine_name' => '',
  );

  // Exported role: Adult Students Content Editor.
  $roles['Adult Students Content Editor'] = array(
    'name' => 'Adult Students Content Editor',
    'weight' => '14',
    'machine_name' => '',
  );

  // Exported role: Alumni Content Editor.
  $roles['Alumni Content Editor'] = array(
    'name' => 'Alumni Content Editor',
    'weight' => '16',
    'machine_name' => '',
  );

  // Exported role: Bursar Content Editor.
  $roles['Bursar Content Editor'] = array(
    'name' => 'Bursar Content Editor',
    'weight' => '20',
    'machine_name' => '',
  );

  // Exported role: Business Content Editor.
  $roles['Business Content Editor'] = array(
    'name' => 'Business Content Editor',
    'weight' => '33',
    'machine_name' => '',
  );

  // Exported role: Chris role.
  $roles['Chris role'] = array(
    'name' => 'Chris role',
    'weight' => '43',
    'machine_name' => '',
  );

  // Exported role: Content Editor Category.
  $roles['Content Editor Category'] = array(
    'name' => 'Content Editor Category',
    'weight' => '3',
    'machine_name' => '',
  );

  // Exported role: Content Editor Multiple Sections.
  $roles['Content Editor Multiple Sections'] = array(
    'name' => 'Content Editor Multiple Sections',
    'weight' => '5',
    'machine_name' => '',
  );

  // Exported role: Content Editor Multiple Sections and Categories.
  $roles['Content Editor Multiple Sections and Categories'] = array(
    'name' => 'Content Editor Multiple Sections and Categories',
    'weight' => '6',
    'machine_name' => '',
  );

  // Exported role: Content Editor Section.
  $roles['Content Editor Section'] = array(
    'name' => 'Content Editor Section',
    'weight' => '4',
    'machine_name' => '',
  );

  // Exported role: Content Editor Single Page.
  $roles['Content Editor Single Page'] = array(
    'name' => 'Content Editor Single Page',
    'weight' => '7',
    'machine_name' => '',
  );

  // Exported role: Copy & Multimedia Content Editor.
  $roles['Copy & Multimedia Content Editor'] = array(
    'name' => 'Copy & Multimedia Content Editor',
    'weight' => '19',
    'machine_name' => '',
  );

  // Exported role: Email Migration Content Editor.
  $roles['Email Migration Content Editor'] = array(
    'name' => 'Email Migration Content Editor',
    'weight' => '8',
    'machine_name' => '',
  );

  // Exported role: Employment Content Editor.
  $roles['Employment Content Editor'] = array(
    'name' => 'Employment Content Editor',
    'weight' => '40',
    'machine_name' => '',
  );

  // Exported role: Engineering Content Editor.
  $roles['Engineering Content Editor'] = array(
    'name' => 'Engineering Content Editor',
    'weight' => '21',
    'machine_name' => '',
  );

  // Exported role: Faculty Council Content Editor.
  $roles['Faculty Council Content Editor'] = array(
    'name' => 'Faculty Council Content Editor',
    'weight' => '32',
    'machine_name' => '',
  );

  // Exported role: Faculty Resources Content Editor.
  $roles['Faculty Resources Content Editor'] = array(
    'name' => 'Faculty Resources Content Editor',
    'weight' => '37',
    'machine_name' => '',
  );

  // Exported role: Financial Aid Content Editor.
  $roles['Financial Aid Content Editor'] = array(
    'name' => 'Financial Aid Content Editor',
    'weight' => '13',
    'machine_name' => '',
  );

  // Exported role: Graduate Admissions Content Editor.
  $roles['Graduate Admissions Content Editor'] = array(
    'name' => 'Graduate Admissions Content Editor',
    'weight' => '18',
    'machine_name' => '',
  );

  // Exported role: HSS Content Editor.
  $roles['HSS Content Editor'] = array(
    'name' => 'HSS Content Editor',
    'weight' => '17',
    'machine_name' => '',
  );

  // Exported role: Honors Content Editor.
  $roles['Honors Content Editor'] = array(
    'name' => 'Honors Content Editor',
    'weight' => '27',
    'machine_name' => '',
  );

  // Exported role: Intercollege CIVCM Content Editor.
  $roles['Intercollege CIVCM Content Editor'] = array(
    'name' => 'Intercollege CIVCM Content Editor',
    'weight' => '12',
    'machine_name' => '',
  );

  // Exported role: Jims Role.
  $roles['Jims Role'] = array(
    'name' => 'Jims Role',
    'weight' => '42',
    'machine_name' => '',
  );

  // Exported role: Learning Resource Content Editor.
  $roles['Learning Resource Content Editor'] = array(
    'name' => 'Learning Resource Content Editor',
    'weight' => '28',
    'machine_name' => '',
  );

  // Exported role: Mail Services Content Editor.
  $roles['Mail Services Content Editor'] = array(
    'name' => 'Mail Services Content Editor',
    'weight' => '34',
    'machine_name' => '',
  );

  // Exported role: Maintenance & Op Content Editor.
  $roles['Maintenance & Op Content Editor'] = array(
    'name' => 'Maintenance & Op Content Editor',
    'weight' => '30',
    'machine_name' => '',
  );

  // Exported role: News & Events Content Editor.
  $roles['News & Events Content Editor'] = array(
    'name' => 'News & Events Content Editor',
    'weight' => '24',
    'machine_name' => '',
  );

  // Exported role: Nicks Role.
  $roles['Nicks Role'] = array(
    'name' => 'Nicks Role',
    'weight' => '41',
    'machine_name' => '',
  );

  // Exported role: Off Campus Housing Content Editor.
  $roles['Off Campus Housing Content Editor'] = array(
    'name' => 'Off Campus Housing Content Editor',
    'weight' => '39',
    'machine_name' => '',
  );

  // Exported role: Operations Content Editor.
  $roles['Operations Content Editor'] = array(
    'name' => 'Operations Content Editor',
    'weight' => '35',
    'machine_name' => '',
  );

  // Exported role: Police Content Editor.
  $roles['Police Content Editor'] = array(
    'name' => 'Police Content Editor',
    'weight' => '31',
    'machine_name' => '',
  );

  // Exported role: Registrar Content Editor.
  $roles['Registrar Content Editor'] = array(
    'name' => 'Registrar Content Editor',
    'weight' => '10',
    'machine_name' => '',
  );

  // Exported role: Research & Outreach Content Editor.
  $roles['Research & Outreach Content Editor'] = array(
    'name' => 'Research & Outreach Content Editor',
    'weight' => '38',
    'machine_name' => '',
  );

  // Exported role: Science Content Editor.
  $roles['Science Content Editor'] = array(
    'name' => 'Science Content Editor',
    'weight' => '25',
    'machine_name' => '',
  );

  // Exported role: Server Health Content Editor.
  $roles['Server Health Content Editor'] = array(
    'name' => 'Server Health Content Editor',
    'weight' => '23',
    'machine_name' => '',
  );

  // Exported role: Student Life Content Editor.
  $roles['Student Life Content Editor'] = array(
    'name' => 'Student Life Content Editor',
    'weight' => '9',
    'machine_name' => '',
  );

  // Exported role: Study Abroad Content Editor.
  $roles['Study Abroad Content Editor'] = array(
    'name' => 'Study Abroad Content Editor',
    'weight' => '29',
    'machine_name' => '',
  );

  // Exported role: Summer Session Content Editor.
  $roles['Summer Session Content Editor'] = array(
    'name' => 'Summer Session Content Editor',
    'weight' => '15',
    'machine_name' => '',
  );

  // Exported role: Telecommunications Content Editor.
  $roles['Telecommunications Content Editor'] = array(
    'name' => 'Telecommunications Content Editor',
    'weight' => '36',
    'machine_name' => '',
  );

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => '2',
    'machine_name' => '',
  );

  return $roles;
}
