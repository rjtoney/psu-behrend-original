<?php
/**
 * @file
 * behrend_menus.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function behrend_menus_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-intranet:node/1153
  $menu_links['menu-intranet:node/1153'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1153',
    'router_path' => 'node/%',
    'link_title' => 'Intranet',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '1',
    'weight' => '0',
  );
  // Exported menu link: menu-intranet:node/1154
  $menu_links['menu-intranet:node/1154'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1154',
    'router_path' => 'node/%',
    'link_title' => 'Computer Center',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1153',
  );
  // Exported menu link: menu-intranet:node/1155
  $menu_links['menu-intranet:node/1155'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1155',
    'router_path' => 'node/%',
    'link_title' => 'copy-multimedia-center',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1153',
  );
  // Exported menu link: menu-intranet:node/1156
  $menu_links['menu-intranet:node/1156'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1156',
    'router_path' => 'node/%',
    'link_title' => 'Faculty Resources',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1153',
  );
  // Exported menu link: menu-intranet:node/1157
  $menu_links['menu-intranet:node/1157'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1157',
    'router_path' => 'node/%',
    'link_title' => 'Untitled Document',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1153',
  );
  // Exported menu link: menu-intranet:node/1158
  $menu_links['menu-intranet:node/1158'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1158',
    'router_path' => 'node/%',
    'link_title' => 'Mail Services',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1153',
  );
  // Exported menu link: menu-intranet:node/1159
  $menu_links['menu-intranet:node/1159'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1159',
    'router_path' => 'node/%',
    'link_title' => 'Maintenance',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1153',
  );
  // Exported menu link: menu-intranet:node/1160
  $menu_links['menu-intranet:node/1160'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1160',
    'router_path' => 'node/%',
    'link_title' => 'Operations',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1153',
  );
  // Exported menu link: menu-intranet:node/1161
  $menu_links['menu-intranet:node/1161'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1161',
    'router_path' => 'node/%',
    'link_title' => 'Site map',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1153',
  );
  // Exported menu link: menu-intranet:node/1162
  $menu_links['menu-intranet:node/1162'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1162',
    'router_path' => 'node/%',
    'link_title' => 'Telecommunications',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1153',
  );
  // Exported menu link: menu-intranet:node/1163
  $menu_links['menu-intranet:node/1163'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1163',
    'router_path' => 'node/%',
    'link_title' => 'Vision and Mission',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1154',
  );
  // Exported menu link: menu-intranet:node/1164
  $menu_links['menu-intranet:node/1164'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1164',
    'router_path' => 'node/%',
    'link_title' => 'Academic Integrity',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1156',
  );
  // Exported menu link: menu-intranet:node/1165
  $menu_links['menu-intranet:node/1165'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1165',
    'router_path' => 'node/%',
    'link_title' => 'Code Blue Phones',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1162',
  );
  // Exported menu link: menu-intranet:node/1166
  $menu_links['menu-intranet:node/1166'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1166',
    'router_path' => 'node/%',
    'link_title' => 'Computer Center',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1154',
  );
  // Exported menu link: menu-intranet:node/1167
  $menu_links['menu-intranet:node/1167'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1167',
    'router_path' => 'node/%',
    'link_title' => 'Computer Labs',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1154',
  );
  // Exported menu link: menu-intranet:node/1168
  $menu_links['menu-intranet:node/1168'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1168',
    'router_path' => 'node/%',
    'link_title' => 'Contact Us',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1155',
  );
  // Exported menu link: menu-intranet:node/1169
  $menu_links['menu-intranet:node/1169'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1169',
    'router_path' => 'node/%',
    'link_title' => 'copy-multimedia-center',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1155',
  );
  // Exported menu link: menu-intranet:node/1170
  $menu_links['menu-intranet:node/1170'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1170',
    'router_path' => 'node/%',
    'link_title' => 'Copyright Information',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1155',
  );
  // Exported menu link: menu-intranet:node/1171
  $menu_links['menu-intranet:node/1171'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1171',
    'router_path' => 'node/%',
    'link_title' => 'Courtesy Phones',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1162',
  );
  // Exported menu link: menu-intranet:node/1172
  $menu_links['menu-intranet:node/1172'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1172',
    'router_path' => 'node/%',
    'link_title' => 'Department Bulk Mail',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1158',
  );
  // Exported menu link: menu-intranet:node/1173
  $menu_links['menu-intranet:node/1173'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1173',
    'router_path' => 'node/%',
    'link_title' => 'Departmental Delivery Times',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1158',
  );
  // Exported menu link: menu-intranet:node/1174
  $menu_links['menu-intranet:node/1174'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1174',
    'router_path' => 'node/%',
    'link_title' => 'Directory Assistance',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1162',
  );
  // Exported menu link: menu-intranet:node/1175
  $menu_links['menu-intranet:node/1175'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1175',
    'router_path' => 'node/%',
    'link_title' => 'Faculty & Staff Services',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1155',
  );
  // Exported menu link: menu-intranet:node/1176
  $menu_links['menu-intranet:node/1176'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1176',
    'router_path' => 'node/%',
    'link_title' => 'Helpful Links',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1158',
  );
  // Exported menu link: menu-intranet:node/1177
  $menu_links['menu-intranet:node/1177'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1177',
    'router_path' => 'node/%',
    'link_title' => 'Helpful Links',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1154',
  );
  // Exported menu link: menu-intranet:node/1178
  $menu_links['menu-intranet:node/1178'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1178',
    'router_path' => 'node/%',
    'link_title' => 'Large Mail Pickup',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1158',
  );
  // Exported menu link: menu-intranet:node/1179
  $menu_links['menu-intranet:node/1179'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1179',
    'router_path' => 'node/%',
    'link_title' => 'Untitled Document',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1180',
  );
  // Exported menu link: menu-intranet:node/1180
  $menu_links['menu-intranet:node/1180'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1180',
    'router_path' => 'node/%',
    'link_title' => 'students',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1153',
  );
  // Exported menu link: menu-intranet:node/1181
  $menu_links['menu-intranet:node/1181'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1181',
    'router_path' => 'node/%',
    'link_title' => 'Mail Services',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1158',
  );
  // Exported menu link: menu-intranet:node/1182
  $menu_links['menu-intranet:node/1182'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1182',
    'router_path' => 'node/%',
    'link_title' => 'Mailing Instructions',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1158',
  );
  // Exported menu link: menu-intranet:node/1183
  $menu_links['menu-intranet:node/1183'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1183',
    'router_path' => 'node/%',
    'link_title' => 'Untitled Document',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1184',
  );
  // Exported menu link: menu-intranet:node/1184
  $menu_links['menu-intranet:node/1184'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1184',
    'router_path' => 'node/%',
    'link_title' => 'faculty-and-staff',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1153',
  );
  // Exported menu link: menu-intranet:node/1185
  $menu_links['menu-intranet:node/1185'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1185',
    'router_path' => 'node/%',
    'link_title' => 'Maintenance',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1159',
  );
  // Exported menu link: menu-intranet:node/1186
  $menu_links['menu-intranet:node/1186'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1186',
    'router_path' => 'node/%',
    'link_title' => 'Meet Me Conferencing',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1162',
  );
  // Exported menu link: menu-intranet:node/1187
  $menu_links['menu-intranet:node/1187'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1187',
    'router_path' => 'node/%',
    'link_title' => 'News Collection',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1154',
  );
  // Exported menu link: menu-intranet:node/1188
  $menu_links['menu-intranet:node/1188'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1188',
    'router_path' => 'node/%',
    'link_title' => 'Off-Campus Mail Vendors',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1158',
  );
  // Exported menu link: menu-intranet:node/1189
  $menu_links['menu-intranet:node/1189'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1189',
    'router_path' => 'node/%',
    'link_title' => 'Untitled Document',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1184',
  );
  // Exported menu link: menu-intranet:node/1190
  $menu_links['menu-intranet:node/1190'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1190',
    'router_path' => 'node/%',
    'link_title' => 'Phone Service & Problems',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1162',
  );
  // Exported menu link: menu-intranet:node/1191
  $menu_links['menu-intranet:node/1191'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1191',
    'router_path' => 'node/%',
    'link_title' => 'Phone Types & Features',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1162',
  );
  // Exported menu link: menu-intranet:node/1192
  $menu_links['menu-intranet:node/1192'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1192',
    'router_path' => 'node/%',
    'link_title' => 'Photography & Videography',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1155',
  );
  // Exported menu link: menu-intranet:node/1193
  $menu_links['menu-intranet:node/1193'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1193',
    'router_path' => 'node/%',
    'link_title' => 'Resources & Services',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1154',
  );
  // Exported menu link: menu-intranet:node/1194
  $menu_links['menu-intranet:node/1194'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1194',
    'router_path' => 'node/%',
    'link_title' => 'Staff',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1159',
  );
  // Exported menu link: menu-intranet:node/1195
  $menu_links['menu-intranet:node/1195'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1195',
    'router_path' => 'node/%',
    'link_title' => 'Student Services',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1155',
  );
  // Exported menu link: menu-intranet:node/1196
  $menu_links['menu-intranet:node/1196'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1196',
    'router_path' => 'node/%',
    'link_title' => 'Telecommunications',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1162',
  );
  // Exported menu link: menu-intranet:node/1197
  $menu_links['menu-intranet:node/1197'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1197',
    'router_path' => 'node/%',
    'link_title' => 'Mail Services',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1158',
  );
  // Exported menu link: menu-intranet:node/1198
  $menu_links['menu-intranet:node/1198'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1198',
    'router_path' => 'node/%',
    'link_title' => 'Consultation & Help',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1154',
  );
  // Exported menu link: menu-intranet:node/1199
  $menu_links['menu-intranet:node/1199'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1199',
    'router_path' => 'node/%',
    'link_title' => 'Teaching Handbook',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1156',
  );
  // Exported menu link: menu-intranet:node/1200
  $menu_links['menu-intranet:node/1200'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1200',
    'router_path' => 'node/%',
    'link_title' => 'Technology Classrooms',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1155',
  );
  // Exported menu link: menu-intranet:node/1201
  $menu_links['menu-intranet:node/1201'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1201',
    'router_path' => 'node/%',
    'link_title' => 'Training',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1154',
  );
  // Exported menu link: menu-intranet:node/1202
  $menu_links['menu-intranet:node/1202'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1202',
    'router_path' => 'node/%',
    'link_title' => 'Video Conferencing',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1155',
  );
  // Exported menu link: menu-intranet:node/1203
  $menu_links['menu-intranet:node/1203'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1203',
    'router_path' => 'node/%',
    'link_title' => 'Voice Mail Services',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1162',
  );
  // Exported menu link: menu-intranet:node/1204
  $menu_links['menu-intranet:node/1204'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1204',
    'router_path' => 'node/%',
    'link_title' => 'Academic Integrity',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1164',
  );
  // Exported menu link: menu-intranet:node/1205
  $menu_links['menu-intranet:node/1205'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1205',
    'router_path' => 'node/%',
    'link_title' => 'ASG Newsletter',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1187',
  );
  // Exported menu link: menu-intranet:node/1206
  $menu_links['menu-intranet:node/1206'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1206',
    'router_path' => 'node/%',
    'link_title' => 'Access Codes',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1207',
  );
  // Exported menu link: menu-intranet:node/1207
  $menu_links['menu-intranet:node/1207'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1207',
    'router_path' => 'node/%',
    'link_title' => 'voice-mail-instructions',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1162',
  );
  // Exported menu link: menu-intranet:node/1208
  $menu_links['menu-intranet:node/1208'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1208',
    'router_path' => 'node/%',
    'link_title' => 'Greetings and Auto Messages',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1207',
  );
  // Exported menu link: menu-intranet:node/1209
  $menu_links['menu-intranet:node/1209'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1209',
    'router_path' => 'node/%',
    'link_title' => 'News Collection',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1187',
  );
  // Exported menu link: menu-intranet:node/1210
  $menu_links['menu-intranet:node/1210'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1210',
    'router_path' => 'node/%',
    'link_title' => 'Policies & Procedures',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1167',
  );
  // Exported menu link: menu-intranet:node/1211
  $menu_links['menu-intranet:node/1211'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1211',
    'router_path' => 'node/%',
    'link_title' => 'Record Messages',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1207',
  );
  // Exported menu link: menu-intranet:node/1212
  $menu_links['menu-intranet:node/1212'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1212',
    'router_path' => 'node/%',
    'link_title' => 'Respond to Messages',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1207',
  );
  // Exported menu link: menu-intranet:node/1213
  $menu_links['menu-intranet:node/1213'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1213',
    'router_path' => 'node/%',
    'link_title' => 'Send All Calls',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1207',
  );
  // Exported menu link: menu-intranet:node/1214
  $menu_links['menu-intranet:node/1214'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1214',
    'router_path' => 'node/%',
    'link_title' => 'Teaching Handbook',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1199',
  );
  // Exported menu link: menu-intranet:node/1215
  $menu_links['menu-intranet:node/1215'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1215',
    'router_path' => 'node/%',
    'link_title' => 'Voice Mail Lists',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1207',
  );
  // Exported menu link: menu-intranet:node/1216
  $menu_links['menu-intranet:node/1216'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1216',
    'router_path' => 'node/%',
    'link_title' => 'Untitled Document',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1218',
  );
  // Exported menu link: menu-intranet:node/1217
  $menu_links['menu-intranet:node/1217'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1217',
    'router_path' => 'node/%',
    'link_title' => 'faculty-resources',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1184',
  );
  // Exported menu link: menu-intranet:node/1218
  $menu_links['menu-intranet:node/1218'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1218',
    'router_path' => 'node/%',
    'link_title' => 'academic-integrity',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1217',
  );
  // Exported menu link: menu-intranet:node/1219
  $menu_links['menu-intranet:node/1219'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1219',
    'router_path' => 'node/%',
    'link_title' => 'Untitled Document',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1221',
  );
  // Exported menu link: menu-intranet:node/1220
  $menu_links['menu-intranet:node/1220'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1220',
    'router_path' => 'node/%',
    'link_title' => 'telecommunications',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1184',
  );
  // Exported menu link: menu-intranet:node/1221
  $menu_links['menu-intranet:node/1221'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1221',
    'router_path' => 'node/%',
    'link_title' => 'voice-mail-instructions',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1220',
  );
  // Exported menu link: menu-intranet:node/1222
  $menu_links['menu-intranet:node/1222'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1222',
    'router_path' => 'node/%',
    'link_title' => 'new-features-for-adobe-connect',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1223',
  );
  // Exported menu link: menu-intranet:node/1223
  $menu_links['menu-intranet:node/1223'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1223',
    'router_path' => 'node/%',
    'link_title' => 'news-items',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1187',
  );
  // Exported menu link: menu-intranet:node/1224
  $menu_links['menu-intranet:node/1224'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1224',
    'router_path' => 'node/%',
    'link_title' => 'Prevent Identity Theft',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1223',
  );
  // Exported menu link: menu-intranet:node/1225
  $menu_links['menu-intranet:node/1225'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1225',
    'router_path' => 'node/%',
    'link_title' => 'protect-your-computer',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1223',
  );
  // Exported menu link: menu-intranet:node/1226
  $menu_links['menu-intranet:node/1226'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1226',
    'router_path' => 'node/%',
    'link_title' => 'Untitled Document',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1221',
  );
  // Exported menu link: menu-intranet:node/1227
  $menu_links['menu-intranet:node/1227'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1227',
    'router_path' => 'node/%',
    'link_title' => 'Untitled Document',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1221',
  );
  // Exported menu link: menu-intranet:node/1228
  $menu_links['menu-intranet:node/1228'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1228',
    'router_path' => 'node/%',
    'link_title' => 'Untitled Document',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1229',
  );
  // Exported menu link: menu-intranet:node/1229
  $menu_links['menu-intranet:node/1229'] = array(
    'menu_name' => 'menu-intranet',
    'link_path' => 'node/1229',
    'router_path' => 'node/%',
    'link_title' => 'teaching-handbook',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/1217',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('ASG Newsletter');
  t('Academic Integrity');
  t('Access Codes');
  t('Code Blue Phones');
  t('Computer Center');
  t('Computer Labs');
  t('Consultation & Help');
  t('Contact Us');
  t('Copyright Information');
  t('Courtesy Phones');
  t('Department Bulk Mail');
  t('Departmental Delivery Times');
  t('Directory Assistance');
  t('Faculty & Staff Services');
  t('Faculty Resources');
  t('Greetings and Auto Messages');
  t('Helpful Links');
  t('Intranet');
  t('Large Mail Pickup');
  t('Mail Services');
  t('Mailing Instructions');
  t('Maintenance');
  t('Meet Me Conferencing');
  t('News Collection');
  t('Off-Campus Mail Vendors');
  t('Operations');
  t('Phone Service & Problems');
  t('Phone Types & Features');
  t('Photography & Videography');
  t('Policies & Procedures');
  t('Prevent Identity Theft');
  t('Record Messages');
  t('Resources & Services');
  t('Respond to Messages');
  t('Send All Calls');
  t('Site map');
  t('Staff');
  t('Student Services');
  t('Teaching Handbook');
  t('Technology Classrooms');
  t('Telecommunications');
  t('Training');
  t('Untitled Document');
  t('Video Conferencing');
  t('Vision and Mission');
  t('Voice Mail Lists');
  t('Voice Mail Services');
  t('academic-integrity');
  t('copy-multimedia-center');
  t('faculty-and-staff');
  t('faculty-resources');
  t('new-features-for-adobe-connect');
  t('news-items');
  t('protect-your-computer');
  t('students');
  t('teaching-handbook');
  t('telecommunications');
  t('voice-mail-instructions');


  return $menu_links;
}
