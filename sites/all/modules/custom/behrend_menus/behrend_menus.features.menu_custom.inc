<?php
/**
 * @file
 * behrend_menus.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function behrend_menus_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-intranet
  $menus['menu-intranet'] = array(
    'menu_name' => 'menu-intranet',
    'title' => 'Intranet',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Intranet');


  return $menus;
}
