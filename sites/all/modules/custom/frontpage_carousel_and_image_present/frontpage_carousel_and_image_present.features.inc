<?php
/**
 * @file
 * frontpage_carousel_and_image_present.features.inc
 */

/**
 * Implements hook_views_api().
 */
function frontpage_carousel_and_image_present_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function frontpage_carousel_and_image_present_image_default_styles() {
  $styles = array();

  // Exported image style: behrend_frontpage.
  $styles['behrend_frontpage'] = array(
    'name' => 'behrend_frontpage',
    'effects' => array(
      9 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 687,
          'height' => 364,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
      10 => array(
        'label' => 'Crop',
        'help' => 'Cropping will remove portions of an image to make it the specified dimensions.',
        'effect callback' => 'image_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_crop_form',
        'summary theme' => 'image_crop_summary',
        'module' => 'image',
        'name' => 'image_crop',
        'data' => array(
          'width' => 687,
          'height' => 364,
          'anchor' => 'center-top',
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}
