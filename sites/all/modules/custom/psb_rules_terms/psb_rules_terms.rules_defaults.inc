<?php
/**
 * @file
 * psb_rules-terms.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function psb_rules_terms_default_rules_configuration() {
  $items = array();
  $items['rules_admin_publishes_node'] = entity_import('rules_config', '{ "rules_admin_publishes_node" : {
      "LABEL" : "Admin Publishes Node",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "node_is_published" : { "node" : [ "node" ] } },
        { "AND" : [
            { "user_has_role" : {
                "account" : [ "node-unchanged:author" ],
                "roles" : { "value" : { "3" : "3" } }
              }
            }
          ]
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "rjt5008@psu.edu",
            "subject" : "[node:author]",
            "message" : "teees",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_content_editor_creates_event'] = entity_import('rules_config', '{ "rules_content_editor_creates_event" : {
      "LABEL" : "Content Editor Creates Event",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "user_has_role" : {
            "account" : [ "node:author" ],
            "roles" : { "value" : { "4" : "4", "5" : "5", "6" : "6", "7" : "7", "8" : "8" } }
          }
        },
        { "AND" : [] },
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "event" : "event" } } } }
      ],
      "DO" : [
        { "mail" : {
            "to" : "rjt5008@psu.edu",
            "subject" : "New [node:content-type] created by [node:author] -- AWAITING APPROVAL",
            "message" : "Event Title:[node:title] by [node:author] and It\\u0027s awaiting approval.\\r\\n\\r\\nClick here to access the newly created Event item [node:url]\\r\\n\\r\\nClick here to review and Approve from the Review Screen\\r\\nhttp:\\/\\/128.118.18.125\\/dashboard\\/content-review",
            "from" : "bdwebmaster@psu.edu",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_content_editor_creates_news'] = entity_import('rules_config', '{ "rules_content_editor_creates_news" : {
      "LABEL" : "Content Editor Creates News",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "user_has_role" : {
            "account" : [ "node:author" ],
            "roles" : { "value" : { "4" : "4", "5" : "5", "6" : "6", "7" : "7", "8" : "8" } }
          }
        },
        { "AND" : [] },
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "news" : "news" } } } }
      ],
      "DO" : [
        { "mail" : {
            "to" : "rjt5008@psu.edu",
            "subject" : "New [node:content-type] created by [node:author] -- AWAITING APPROVAL",
            "message" : "News Title:[node:title] by [node:author] and It\\u0027s awaiting approval.\\r\\n\\r\\nClick here to access the newly created news item [node:url]\\r\\n\\r\\nClick here to review and Approve from the Review Screen\\r\\nhttp:\\/\\/128.118.18.125\\/dashboard\\/content-review",
            "from" : "bdwebmaster@psu.edu",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_content_editor_creates_node'] = entity_import('rules_config', '{ "rules_content_editor_creates_node" : {
      "LABEL" : "Content Editor Creates Basic Page",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_presave" ],
      "IF" : [
        { "NOT user_has_role" : { "account" : [ "node:author" ], "roles" : { "value" : { "3" : "3" } } } }
      ],
      "DO" : [
        { "mail" : {
            "to" : "richard.john.toney@gmail.com",
            "subject" : "New [node:content-type] created by [node:author] -- AWAITING APPROVAL",
            "message" : "Page Title:[node:title] by [node:author] and It\\u0027s awaiting approval.\\r\\n\\r\\nYou may moderate the content here\\r\\nhttp:\\/\\/128.118.18.125\\/admin\\/content\\/modr8\\r\\n",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_deletes_node'] = entity_import('rules_config', '{ "rules_deletes_node" : {
      "LABEL" : "Deletes Node",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "NOT user_has_role" : {
            "account" : [ "node:author" ],
            "roles" : { "value" : { "1" : "1", "3" : "3" } },
            "operation" : "OR"
          }
        },
        { "AND" : [
            { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_basic_page_status" } },
            { "data_is" : { "data" : [ "node:field-basic-page-status" ], "value" : "2" } }
          ]
        }
      ],
      "DO" : [
        { "entity_delete" : { "data" : [ "node" ] } },
        { "drupal_message" : { "message" : "[node:title] has been deleted", "repeat" : 0 } },
        { "redirect" : { "url" : "dashboard" } }
      ]
    }
  }');
  $items['rules_inform_user_page_is_unpublished'] = entity_import('rules_config', '{ "rules_inform_user_page_is_unpublished" : {
      "LABEL" : "Inform user page is unpublished",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_view" ],
      "IF" : [
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "2" : "2" } },
            "operation" : "OR"
          }
        },
        { "AND" : [] },
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_basic_page_status" } },
        { "AND" : [] },
        { "data_is" : { "data" : [ "node:field-basic-page-status" ], "value" : "1" } }
      ],
      "DO" : [
        { "drupal_message" : {
            "message" : "This page is currently Unpublished, which means anonymous users can not view it.",
            "repeat" : 0
          }
        }
      ]
    }
  }');
  $items['rules_republish_node'] = entity_import('rules_config', '{ "rules_republish_node" : {
      "LABEL" : "Republish node",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "NOT user_has_role" : {
            "account" : [ "node:author" ],
            "roles" : { "value" : { "1" : "1", "3" : "3" } },
            "operation" : "OR"
          }
        },
        { "AND" : [
            { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_basic_page_status" } },
            { "data_is" : { "data" : [ "node:field-basic-page-status" ], "value" : "0" } }
          ]
        }
      ],
      "DO" : [
        { "drupal_message" : {
            "message" : "[node:title] is now Published and is accessible for anonymous users.  To Unpublish the page, simply edit it and change the \\u003Cem\\u003EStatus\\u003C\\/em\\u003E dropdown box",
            "repeat" : 0
          }
        },
        { "node_publish" : { "node" : [ "node" ] } }
      ]
    }
  }');
  $items['rules_send_an_email_to_editors'] = entity_import('rules_config', '{ "rules_send_an_email_to_editors" : {
      "LABEL" : "Send an email to Editors",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : {
                "academic_program" : "academic_program",
                "article" : "article",
                "page" : "page",
                "event" : "event",
                "news" : "news"
              }
            }
          }
        },
        { "AND" : [
            { "NOT user_has_role" : {
                "account" : [ "node:author" ],
                "roles" : { "value" : { "1" : "1", "3" : "3" } },
                "operation" : "OR"
              }
            },
            { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_basic_page_status" } },
            { "data_is" : { "data" : [ "node:field-basic-page-status" ], "value" : "3" } }
          ]
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "node:author:mail" ],
            "subject" : "[node:content-type] : [node:title] is pending approval!",
            "message" : "Content has is being reviewed.\\r\\n\\r\\nYou will be notified once approved.\\r\\n\\r\\nIf you would like to make changes to [node:title], you may do so here Link: [site:url]dashboard\\/pending-approval#overlay=node\\/[node:nid]\\/edit",
            "from" : "bdwebmaster@psu.edu",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_unpublish_confirmation'] = entity_import('rules_config', '{ "rules_unpublish_confirmation" : {
      "LABEL" : "Unpublish Confirmation",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "NOT node_is_published" : { "node" : [ "node" ] } },
        { "AND" : [
            { "user_has_role" : {
                "account" : [ "node:author" ],
                "roles" : { "value" : {
                    "4" : "4",
                    "5" : "5",
                    "6" : "6",
                    "7" : "7",
                    "8" : "8",
                    "9" : "9",
                    "10" : "10",
                    "11" : "11",
                    "12" : "12",
                    "13" : "13",
                    "14" : "14",
                    "15" : "15",
                    "16" : "16",
                    "17" : "17",
                    "18" : "18",
                    "19" : "19",
                    "20" : "20",
                    "21" : "21",
                    "22" : "22",
                    "23" : "23",
                    "24" : "24",
                    "25" : "25",
                    "26" : "26",
                    "27" : "27",
                    "28" : "28",
                    "29" : "29",
                    "30" : "30",
                    "31" : "31",
                    "32" : "32",
                    "33" : "33",
                    "34" : "34",
                    "35" : "35",
                    "36" : "36",
                    "37" : "37",
                    "38" : "38",
                    "39" : "39",
                    "40" : "40",
                    "41" : "41"
                  }
                },
                "operation" : "OR"
              }
            }
          ]
        }
      ],
      "DO" : [
        { "drupal_message" : { "message" : "[node:title] is Unpublished and not accessible for anonymous users." } }
      ]
    }
  }');
  $items['rules_unpublish_content'] = entity_import('rules_config', '{ "rules_unpublish_content" : {
      "LABEL" : "Unpublish Content",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_delete" ],
      "DO" : []
    }
  }');
  $items['rules_unpublish_node'] = entity_import('rules_config', '{ "rules_unpublish_node" : {
      "LABEL" : "Unpublish Node",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "NOT user_has_role" : {
            "account" : [ "node:author" ],
            "roles" : { "value" : { "1" : "1", "3" : "3" } },
            "operation" : "OR"
          }
        },
        { "AND" : [
            { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_basic_page_status" } },
            { "data_is" : { "data" : [ "node:field-basic-page-status" ], "value" : "1" } }
          ]
        }
      ],
      "DO" : [
        { "node_unpublish" : { "node" : [ "node" ] } },
        { "drupal_message" : {
            "message" : "[node:title] is now Unpublished and is not accessible for anonymous users.  To Publish the page, simply edit it and change the \\u003Cem\\u003EStatus\\u003C\\/em\\u003E dropdown box",
            "repeat" : 0
          }
        }
      ]
    }
  }');
  return $items;
}
