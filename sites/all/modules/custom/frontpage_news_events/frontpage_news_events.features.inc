<?php
/**
 * @file
 * frontpage_news_events.features.inc
 */

/**
 * Implements hook_views_api().
 */
function frontpage_news_events_views_api() {
  return array("api" => "3.0");
}
