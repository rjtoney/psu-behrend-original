<?php
/**
 * @file
 * psb_formatters.custom_formatters.inc
 */

/**
 * Implements hook_custom_formatters_defaults().
 */
function psb_formatters_custom_formatters_defaults() {
  $export = array();

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'date_with_am_pm';
  $formatter->label = 'Date with AM & PM';
  $formatter->description = '';
  $formatter->mode = 'php';
  $formatter->field_types = 'field_event_date';
  $formatter->code = 'date("F j, Y, g:i a");';
  $formatter->fapi = '';
  $export['date_with_am_pm'] = $formatter;

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'employee_profile';
  $formatter->label = 'Employee Profile';
  $formatter->description = '';
  $formatter->mode = 'token';
  $formatter->field_types = 'text';
  $formatter->code = '[node:field-employee-prefix] [node:title] [node:field-employee-suffiix]';
  $formatter->fapi = '';
  $export['employee_profile'] = $formatter;

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'soars_registration_button';
  $formatter->label = 'soars registration button';
  $formatter->description = '';
  $formatter->mode = 'token';
  $formatter->field_types = 'text';
  $formatter->code = '<a style="color:white;" href="[node:field_cwp_course_soar_url]"><button style="color:white;" class="btn btn-info">Register Now</button></a>';
  $formatter->fapi = '';
  $export['soars_registration_button'] = $formatter;

  return $export;
}
