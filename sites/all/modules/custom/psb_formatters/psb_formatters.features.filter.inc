<?php
/**
 * @file
 * psb_formatters.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function psb_formatters_filter_default_formats() {
  $formats = array();

  // Exported format: export
  $formats['export'] = array(
    'format' => 'export',
    'name' => 'export',
    'cache' => '1',
    'status' => '1',
    'weight' => '0',
    'filters' => array(
      'filter_html_escape' => array(
        'weight' => '-10',
        'status' => '1',
        'settings' => array(),
      ),
    ),
  );

  // Exported format: PHP code
  $formats['php_code'] = array(
    'format' => 'php_code',
    'name' => 'PHP code',
    'cache' => '1',
    'status' => '1',
    'weight' => '11',
    'filters' => array(),
  );

  // Exported format: Plain text
  $formats['plain_text'] = array(
    'format' => 'plain_text',
    'name' => 'Plain text',
    'cache' => '1',
    'status' => '1',
    'weight' => '10',
    'filters' => array(
      'filter_html_escape' => array(
        'weight' => '0',
        'status' => '1',
        'settings' => array(),
      ),
      'filter_url' => array(
        'weight' => '1',
        'status' => '1',
        'settings' => array(
          'filter_url_length' => 72,
        ),
      ),
      'filter_autop' => array(
        'weight' => '2',
        'status' => '1',
        'settings' => array(),
      ),
    ),
  );

  // Exported format: Unfiltered HTML
  $formats['unfiltered_html'] = array(
    'format' => 'unfiltered_html',
    'name' => 'Unfiltered HTML',
    'cache' => '1',
    'status' => '1',
    'weight' => '0',
    'filters' => array(),
  );

  return $formats;
}
