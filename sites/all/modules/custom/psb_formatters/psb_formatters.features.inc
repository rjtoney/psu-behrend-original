<?php
/**
 * @file
 * psb_formatters.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function psb_formatters_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "custom_formatters" && $api == "custom_formatters") {
    return array("version" => "2");
  }
  list($module, $api) = func_get_args();
  if ($module == "linkit" && $api == "linkit_profiles") {
    return array("version" => "1");
  }
}
