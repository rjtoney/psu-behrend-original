<?php
/**
 * @file
 * psb_content-types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function psb_content-types_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "custom_formatters" && $api == "custom_formatters") {
    return array("version" => "2");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function psb_content-types_node_info() {
  $items = array(
    'academic_program' => array(
      'name' => t('Academic Program'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'behrend_employee_profile' => array(
      'name' => t('Employeer Profile'),
      'base' => 'node_content',
      'description' => t('Used for Faculty and Staff Profiles'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'cwp_course' => array(
      'name' => t('CWP Course'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Course Title'),
      'help' => '',
    ),
    'event' => array(
      'name' => t('Event'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'news' => array(
      'name' => t('News'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
