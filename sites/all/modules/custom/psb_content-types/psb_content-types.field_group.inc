<?php
/**
 * @file
 * psb_content-types.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function psb_content-types_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_biography|node|behrend_employee_profile|default';
  $field_group->group_name = 'group_biography';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Biography',
    'weight' => '36',
    'children' => array(
      0 => 'body',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_biography|node|behrend_employee_profile|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_biography|node|behrend_employee_profile|form';
  $field_group->group_name = 'group_biography';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Biography',
    'weight' => '36',
    'children' => array(
      0 => 'body',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_biography|node|behrend_employee_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_biography|node|behrend_employee_profile|rss';
  $field_group->group_name = 'group_biography';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'rss';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Biography',
    'weight' => '36',
    'children' => array(
      0 => 'body',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_biography|node|behrend_employee_profile|rss'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_biography|node|behrend_employee_profile|teaser';
  $field_group->group_name = 'group_biography';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'teaser';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Biography',
    'weight' => '36',
    'children' => array(
      0 => 'body',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_biography|node|behrend_employee_profile|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_employee_profile_card|node|behrend_employee_profile|default';
  $field_group->group_name = 'group_employee_profile_card';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Profile',
    'weight' => '3',
    'children' => array(
      0 => 'group_biography',
      1 => 'group_teaching',
      2 => 'group_research',
      3 => 'group_publications',
      4 => 'group_service',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_employee_profile_card|node|behrend_employee_profile|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_employee_profile_card|node|behrend_employee_profile|form';
  $field_group->group_name = 'group_employee_profile_card';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Profile',
    'weight' => '8',
    'children' => array(
      0 => 'group_biography',
      1 => 'group_teaching',
      2 => 'group_research',
      3 => 'group_publications',
      4 => 'group_service',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_employee_profile_card|node|behrend_employee_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_employee_profile_card|node|behrend_employee_profile|rss';
  $field_group->group_name = 'group_employee_profile_card';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'rss';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Profile',
    'weight' => '12',
    'children' => array(
      0 => 'group_biography',
      1 => 'group_teaching',
      2 => 'group_research',
      3 => 'group_publications',
      4 => 'group_service',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_employee_profile_card|node|behrend_employee_profile|rss'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_employee_profile_card|node|behrend_employee_profile|teaser';
  $field_group->group_name = 'group_employee_profile_card';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Profile',
    'weight' => '8',
    'children' => array(
      0 => 'group_biography',
      1 => 'group_teaching',
      2 => 'group_research',
      3 => 'group_publications',
      4 => 'group_service',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_employee_profile_card|node|behrend_employee_profile|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_event_information|node|event|form';
  $field_group->group_name = 'group_event_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Event Information',
    'weight' => '2',
    'children' => array(
      0 => 'field_event_dates',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_event_information|node|event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_event_info|node|event|form';
  $field_group->group_name = 'group_event_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Event Contact Info',
    'weight' => '4',
    'children' => array(
      0 => 'field_event_contact_name',
      1 => 'field_event_contact_email',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_event_info|node|event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ignore|node|event|form';
  $field_group->group_name = 'group_ignore';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'IGNORE',
    'weight' => '5',
    'children' => array(
      0 => 'field_event_date',
      1 => 'field_event_contact_phone',
      2 => 'redirect',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_ignore|node|event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile_card|node|behrend_employee_profile|default';
  $field_group->group_name = 'group_profile_card';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '2',
    'children' => array(
      0 => 'field_office_location_employee',
      1 => 'field_employee_phone',
      2 => 'field_employee_fax',
      3 => 'field_employee_email',
      4 => 'field_employee_title',
      5 => 'field_employee_prefix',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_profile_card|node|behrend_employee_profile|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_publications|node|behrend_employee_profile|default';
  $field_group->group_name = 'group_publications';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Publications',
    'weight' => '39',
    'children' => array(
      0 => 'field_employee_publications',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_publications|node|behrend_employee_profile|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_publications|node|behrend_employee_profile|form';
  $field_group->group_name = 'group_publications';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Publications',
    'weight' => '39',
    'children' => array(
      0 => 'field_employee_publications',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_publications|node|behrend_employee_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_publications|node|behrend_employee_profile|rss';
  $field_group->group_name = 'group_publications';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'rss';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Publications',
    'weight' => '39',
    'children' => array(
      0 => 'field_employee_publications',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_publications|node|behrend_employee_profile|rss'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_publications|node|behrend_employee_profile|teaser';
  $field_group->group_name = 'group_publications';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'teaser';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Publications',
    'weight' => '39',
    'children' => array(
      0 => 'field_employee_publications',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_publications|node|behrend_employee_profile|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_research|node|behrend_employee_profile|default';
  $field_group->group_name = 'group_research';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Research',
    'weight' => '38',
    'children' => array(
      0 => 'field_employee_research',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_research|node|behrend_employee_profile|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_research|node|behrend_employee_profile|form';
  $field_group->group_name = 'group_research';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Research',
    'weight' => '38',
    'children' => array(
      0 => 'field_employee_research',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_research|node|behrend_employee_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_research|node|behrend_employee_profile|rss';
  $field_group->group_name = 'group_research';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'rss';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Research',
    'weight' => '38',
    'children' => array(
      0 => 'field_employee_research',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_research|node|behrend_employee_profile|rss'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_research|node|behrend_employee_profile|teaser';
  $field_group->group_name = 'group_research';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'teaser';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Research',
    'weight' => '38',
    'children' => array(
      0 => 'field_employee_research',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_research|node|behrend_employee_profile|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_service|node|behrend_employee_profile|default';
  $field_group->group_name = 'group_service';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Service',
    'weight' => '40',
    'children' => array(
      0 => 'field_employee_service',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_service|node|behrend_employee_profile|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_service|node|behrend_employee_profile|form';
  $field_group->group_name = 'group_service';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Service',
    'weight' => '40',
    'children' => array(
      0 => 'field_employee_service',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_service|node|behrend_employee_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_service|node|behrend_employee_profile|rss';
  $field_group->group_name = 'group_service';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'rss';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Service',
    'weight' => '40',
    'children' => array(
      0 => 'field_employee_service',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_service|node|behrend_employee_profile|rss'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_service|node|behrend_employee_profile|teaser';
  $field_group->group_name = 'group_service';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'teaser';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Service',
    'weight' => '40',
    'children' => array(
      0 => 'field_employee_service',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_service|node|behrend_employee_profile|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_teaching|node|behrend_employee_profile|default';
  $field_group->group_name = 'group_teaching';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Teaching',
    'weight' => '37',
    'children' => array(
      0 => 'field_employee_teaching',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_teaching|node|behrend_employee_profile|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_teaching|node|behrend_employee_profile|form';
  $field_group->group_name = 'group_teaching';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Teaching',
    'weight' => '37',
    'children' => array(
      0 => 'field_employee_teaching',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_teaching|node|behrend_employee_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_teaching|node|behrend_employee_profile|rss';
  $field_group->group_name = 'group_teaching';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'rss';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Teaching',
    'weight' => '37',
    'children' => array(
      0 => 'field_employee_teaching',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_teaching|node|behrend_employee_profile|rss'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_teaching|node|behrend_employee_profile|teaser';
  $field_group->group_name = 'group_teaching';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'behrend_employee_profile';
  $field_group->mode = 'teaser';
  $field_group->parent_name = 'group_employee_profile_card';
  $field_group->data = array(
    'label' => 'Teaching',
    'weight' => '37',
    'children' => array(
      0 => 'field_employee_teaching',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_teaching|node|behrend_employee_profile|teaser'] = $field_group;

  return $export;
}
