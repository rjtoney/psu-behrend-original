<?php
/**
 * @file
 * student_life_page.features.inc
 */

/**
 * Implements hook_node_info().
 */
function student_life_page_node_info() {
  $items = array(
    'student_life_page' => array(
      'name' => t('Student Life Page'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
