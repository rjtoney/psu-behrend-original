<?php
/**
 * @file
 * page_plus.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function page_plus_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_placement|node|page_plus|form';
  $field_group->group_name = 'group_placement';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page_plus';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Placement',
    'weight' => '3',
    'children' => array(
      0 => 'field_page_plus_category',
      1 => 'field_page_plus_section',
      2 => 'field_page_plus_sub_section',
      3 => 'field_page_plus_sub_section_1',
      4 => 'field_page_plus_sub_section_2',
      5 => 'field_page_plus_sub_section_3',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Placement',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_placement|node|page_plus|form'] = $field_group;

  return $export;
}
