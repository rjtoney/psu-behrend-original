<?php
/**
 * @file
 * page_plus.features.inc
 */

/**
 * Implements hook_node_info().
 */
function page_plus_node_info() {
  $items = array(
    'page_plus' => array(
      'name' => t('Page +'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
