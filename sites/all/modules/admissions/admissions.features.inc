<?php
/**
 * @file
 * admissions.features.inc
 */

/**
 * Implements hook_node_info().
 */
function admissions_node_info() {
  $items = array(
    'admissions_page' => array(
      'name' => t('Admissions page'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
