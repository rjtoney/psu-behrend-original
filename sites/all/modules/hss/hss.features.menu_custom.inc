<?php
/**
 * @file
 * hss.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function hss_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-hss
  $menus['menu-hss'] = array(
    'menu_name' => 'menu-hss',
    'title' => 'hss',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('hss');


  return $menus;
}
