<?php
/**
 * @file
 * hss.features.node_export_features.inc
 */

/**
 * Implements hook_node_export_features_default().
 */
function hss_node_export_features_default() {
  // You do not have permission to perform a Node export on one or more of these nodes.  No nodes exported.
  $node_export = array();
  return $node_export;
}
