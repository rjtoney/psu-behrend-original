<?php
/**
 * @file
 * user_fields_final.custom_formatters.inc
 */

/**
 * Implements hook_custom_formatters_defaults().
 */
function user_fields_final_custom_formatters_defaults() {
  $export = array();

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'faculty_directory';
  $formatter->label = 'Faculty Directory';
  $formatter->description = '';
  $formatter->mode = 'token';
  $formatter->field_types = 'text';
  $formatter->code = '<h1><span>[user:field_user_prefix] [user:field_user_full_name] [user:field_user_suffix]</span></h1>';
  $formatter->fapi = '';
  $export['faculty_directory'] = $formatter;

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'phone_profile';
  $formatter->label = 'Phone Profile';
  $formatter->description = '';
  $formatter->mode = 'token';
  $formatter->field_types = 'number_integer';
  $formatter->code = '<a href="tel:1814898[user:field_user_phone]">814-898-[user:field_user_phone]</a>';
  $formatter->fapi = '';
  $export['phone_profile'] = $formatter;

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'profile_image';
  $formatter->label = 'Profile Image';
  $formatter->description = '';
  $formatter->mode = 'token';
  $formatter->field_types = 'image';
  $formatter->code = '
<img class="pull-left" src="[user:field_user_image]" title="[user:field_user_full_name]">';
  $formatter->fapi = '';
  $export['profile_image'] = $formatter;

  return $export;
}
