<?php
/**
 * @file
 * user_fields_final.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function user_fields_final_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_biography|user|user|default';
  $field_group->group_name = 'group_biography';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_standard_pages';
  $field_group->data = array(
    'label' => 'Biography',
    'weight' => '10',
    'children' => array(
      0 => 'field_user_biography',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_biography|user|user|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_event_information|node|event|form';
  $field_group->group_name = 'group_event_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Event Information',
    'weight' => '3',
    'children' => array(
      0 => 'event_ends',
      1 => 'event_starts',
      2 => 'field_event_location',
      3 => 'location_event',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_event_information|node|event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_event_info|node|event|form';
  $field_group->group_name = 'group_event_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Event Contact Info',
    'weight' => '5',
    'children' => array(
      0 => 'field_contact_email',
      1 => 'field_event_contact_name',
      2 => 'field_event_contact_phone',
      3 => 'field_event_url',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_event_info|node|event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_flags|node|page_plus|form';
  $field_group->group_name = 'group_flags';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page_plus';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Add to sidebars',
    'weight' => '6',
    'children' => array(
      0 => 'flag',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Add to sidebars',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_flags|node|page_plus|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ignore|node|event|form';
  $field_group->group_name = 'group_ignore';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'IGNORE',
    'weight' => '12',
    'children' => array(
      0 => 'field_event_category',
      1 => 'field_event_ems_url',
      2 => 'field_event_og_id',
      3 => 'field_event_year',
      4 => 'title_field',
      5 => 'path',
      6 => 'redirect',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_ignore|node|event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_outreach|user|user|default';
  $field_group->group_name = 'group_outreach';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_standard_pages';
  $field_group->data = array(
    'label' => 'Outreach',
    'weight' => '31',
    'children' => array(
      0 => 'field_user_outreach',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Outreach',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_outreach|user|user|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_placement|node|page_plus|form';
  $field_group->group_name = 'group_placement';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page_plus';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Placement',
    'weight' => '3',
    'children' => array(
      0 => 'field_page_plus_category',
      1 => 'field_page_plus_section',
      2 => 'field_page_plus_sub_section',
      3 => 'field_page_plus_sub_section_1',
      4 => 'field_page_plus_sub_section_2',
      5 => 'field_page_plus_sub_section_3',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Placement',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_placement|node|page_plus|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_psb_profile_card|user|user|default';
  $field_group->group_name = 'group_psb_profile_card';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Penn State Behrend Profile Card',
    'weight' => '1',
    'children' => array(
      0 => 'field_psu_email',
      1 => 'field_user_image',
      2 => 'field_user_office_location',
      3 => 'field_user_phone',
      4 => 'field_user_psb_work_unit',
      5 => 'field_user_psu_job_title',
      6 => 'field_user_suffix',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_psb_profile_card|user|user|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_publications|user|user|default';
  $field_group->group_name = 'group_publications';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_standard_pages';
  $field_group->data = array(
    'label' => 'Publications',
    'weight' => '14',
    'children' => array(
      0 => 'field_user_publications',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_publications|user|user|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_research|user|user|default';
  $field_group->group_name = 'group_research';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_standard_pages';
  $field_group->data = array(
    'label' => 'Research',
    'weight' => '12',
    'children' => array(
      0 => 'field_user_research',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_research|user|user|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_service_page|user|user|default';
  $field_group->group_name = 'group_service_page';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_standard_pages';
  $field_group->data = array(
    'label' => 'Service',
    'weight' => '13',
    'children' => array(
      0 => 'field_user_service',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_service_page|user|user|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_standard_pages|user|user|default';
  $field_group->group_name = 'group_standard_pages';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Faculty Standard Information',
    'weight' => '2',
    'children' => array(
      0 => 'group_biography',
      1 => 'group_outreach',
      2 => 'group_publications',
      3 => 'group_research',
      4 => 'group_service_page',
      5 => 'group_teaching',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_standard_pages|user|user|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_teaching|user|user|default';
  $field_group->group_name = 'group_teaching';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_standard_pages';
  $field_group->data = array(
    'label' => 'Teaching',
    'weight' => '11',
    'children' => array(
      0 => 'field_user_teaching',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_teaching|user|user|default'] = $field_group;

  return $export;
}
