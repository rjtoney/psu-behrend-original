<?php 

/**
 * @file
 * Class definition of FeedsProfileProcessor.
 * @author : Teddy NICOLLE
 * 
 * NB: in beta version : 
 * 
 *  Replace option doesn't work because entitySave method crashes when this option is enabled
 *  Update option works
 *  
 * @todo : search the reasons why ($entity is in fact $target_item in map method. $target_item seems
 *   to be an stdClass object...instead of profile2...)
 * 
 */

/**
 * Creates nodes from feed items.
 */
class FeedsProfileProcessor extends FeedsProcessor {
  /**
   * Define entity type.
   */
  public function entityType() {
    return 'profile2';
  }

  /**
   * Implements parent::entityInfo().
   */
  protected function entityInfo() {
    $info = parent::entityInfo();
    $info['label plural'] = t('Profiles');
    return $info;
  }

  /**
   * Creates a new Profile2 in memory and returns it.
   */
  protected function newEntity(FeedsSource $source) {
    
  	$profile=new Profile(array('type' => $this->config['profile2_type'], 'uid' => NULL));
  	   
    return $profile;
  }

  /**
   * Loads an existing profile2.
   *
   * If the update existing method is not FEEDS_UPDATE_EXISTING, only the profile
   * table will be loaded, foregoing the profile2_load API for better performance.
   */
  protected function entityLoad(FeedsSource $source, $pid) {
    
  	if ($this->config['update_existing'] == FEEDS_UPDATE_EXISTING) {
      $profile = profile2_load($pid,TRUE);
    }
    else {
      // We're replacing the existing profile. Only save the absolutely necessary.
      $profile = db_query("SELECT pid,type,uid FROM {profile} WHERE pid = :pid", array(':pid' => $pid))->fetchObject();
     
    }
  
    return $profile;
  }

  /**
   * Save a profile2.
   */
  public function entitySave($entity) {
  	
  	//$values=(array)$entity;
    
    //profile2_save(new Profile($values));
    
  	dsm($entity);
    profile2_save($entity);
  }
  

  /**
   * Delete a series of profile2.
   */
  protected function entityDeleteMultiple($pids) {
    profile2_delete_multiple($pids);
  }

  
  
  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    $types = profile2_get_types();
    $type = isset($types['main']) ? '' : key($types);
    return array(
      'profile2_type' => $type,
      'expire' => FEEDS_EXPIRE_NEVER,
     
    ) + parent::configDefaults();
  }

  
  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    //$types = profile2_get_types();
    $p2_types=profile2_get_types();
    $types=array();
    
  	foreach($p2_types as $type=>$obj){
  		$types[$type]=$obj->label;
  	}
    //array_walk($types, 'check_plain');
    $form = parent::configForm($form_state);
    $form['profile2_type'] = array(
      '#type' => 'select',
      '#title' => t('Profile2 type'),
      '#description' => t('Select the Profile2 type for the profiles to be created. <strong>Note:</strong> Users with "import !feed_id feeds" permissions will be able to <strong>import</strong> profiles of the profile2 type selected here regardless of the profiles2 level permissions. Further, users with "clear !feed_id permissions" will be able to <strong>delete</strong> imported profiles regardless of their profile2 level permissions.', array('!feed_id' => $this->id)),
      '#options' => $types,
      '#default_value' => $this->config['profile2_type'],
    );
    
    $form['update_existing']['#options'] = array(
      FEEDS_SKIP_EXISTING => 'Do not update existing profiles',
      FEEDS_REPLACE_EXISTING => 'Replace existing profiles',
      FEEDS_UPDATE_EXISTING => 'Update existing profiles (slower than replacing them)',
     
    );
    return $form;
  }

  /**
   * Override parent::configFormValidate().
   */
  public function configFormValidate(&$values) {
    parent::configFormValidate($values);
  }

  /**
   * Reschedule if expiry time changes.
   */
  public function configFormSubmit(&$values) {
//    if ($this->config['expire'] != $values['expire']) {
//      feeds_reschedule($this->id);
//    }
    parent::configFormSubmit($values);
  }

  /**
   * Override setTargetElement to operate on a target item that is a profile.
   */
  public function setTargetElement(FeedsSource $source, $target_profile2, $target_element, $value) {
    switch ($target_element) {
        case 'feeds_source':
        // Get the class of the feed profile2 importer's fetcher and set the source
        // property
//        if ($id = feeds_get_importer_id($this->config['profile2_type'])) {
//          $class = get_class(feeds_importer($id)->fetcher);
//          $target_profile2->feeds[$class]['source'] = $value;
//          // This effectively suppresses 'import on submission' feature.
//         
//          $target_profile2->feeds['suppress_import'] = TRUE;
//        }     
        break;
        case 'uid':
        	if (!empty($value)) $target_profile2->uid=$value;
        break;
        case 'ownername':
        	if (!empty($value)) {
        		$user=user_load_by_name($value);
        		$target_profile2->uid=$user->uid;
        	}
      default:
        parent::setTargetElement($source, $target_profile2, $target_element, $value);
        break;
    }
  }

  /**
   * Return available mapping targets.
   * 
   * NB:To use Update/replace, we need pid, uid, ownername to be unique
   * 
   */
  public function getMappingTargets() {
    $type = profile2_get_types($this->config['profile2_type']);
    $targets = parent::getMappingTargets();
    if ($type->label) {
      $targets['label'] = array(
        'name' => t('Label'),
        'description ' => t('The label of the profile2.'),
      );
    }
    $targets += array(
      'pid' => array(
        'name' => t('Profile2 ID'),
        'description' => t('The id of the profile. NOTE: use this feature with care because ids are generally assigned by drupal'),
        'optional_unique' => TRUE,
      ),
      'uid' => array(
        'name' => t('Owner ID'),
        'description' => t('The Drupal user ID of the profile2 owner.'),
        'optional_unique' => TRUE,
      ),
      'ownername' => array(
        'name' => t('Owner name'),
        'description' => t('The Drupal user name of the profile2 owner.'),
        'unique' => TRUE,
      ),
 
    );
    // If the target profile2 type is a Feed Profile, expose its source field.
   

    // Let other modules expose mapping targets.
    self::loadMappers();
    feeds_alter('feeds_processor_targets', $targets, 'profile2', $this->config['profile2_type']);
    dsm($targets);
    return $targets;
  }

  /**
   * Get pid of an existing feed item profile if available.
   */
  protected function existingEntityId(FeedsSource $source, FeedsParserResult $result) {
    if ($pid = parent::existingEntityId($source, $result)) {
     
    	return $pid;
    }
    //dsm($this->id);
    //$parser = feeds_importer($this->id)->parser;
    //dsm($parser);
    //dsm($this->config);
    // Iterate through all unique targets and test whether they do already
    // exist in the database.
    
    
    foreach ($this->uniqueTargets($source, $result) as $target => $value) {
     switch ($target) {
        case 'pid':
          $pid = db_query("SELECT pid FROM {profile} WHERE pid = :pid", array(':pid' => $value))->fetchField();
          break;
        
        case 'uid':
          $pid = db_query("SELECT pid FROM {profile} WHERE uid = :uid", array(':uid' => $value))->fetchField();
          break;
        case 'ownername':
        	$uid=user_load_by_name($value);
        	if($uid){
        	  $pid = db_query("SELECT pid FROM {profile} WHERE uid = :uid", array(':uid' => $uid))->fetchField();
        	}
          break;
        
        case 'feeds_source':
          print_r('feed_source');
           //if ($id = feeds_get_importer_id($this->config['profile2_type'])) {
//            $pid = db_query("SELECT fs.feed_pid FROM {profile} p JOIN {feeds_source} fs ON p.pid = fs.feed_pid WHERE fs.id = :id AND fs.source = :source", array(':id' => $id, ':source' => $value))->fetchField();
//          }
          break;
      }
      if ($pid) {
        // Return with the first pid found.
        return $pid;
      }
    }
    return 0;
  }
}
