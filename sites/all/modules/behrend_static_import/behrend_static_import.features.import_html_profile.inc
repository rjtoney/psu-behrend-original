<?php
/**
 * @file
 * behrend_static_import.features.import_html_profile.inc
 */

/**
 * Implements hook_import_html_profile_defaults().
 */
function behrend_static_import_import_html_profile_defaults() {
  $import_html_profiles = array();

  // Exported setting (only the non-default values): About the College
  $import_html_profiles['About the College'] = array(
    'profile_id' => 'About the College',
    'translation_template' => FALSE,
    'content_tag_id' => 'parent-fieldname-text',
    'content_type' => 'page_plus',
    'file_exclusions' => '^_
/_
CVS
^\\.
/\\.',
    'relink_files' => TRUE,
    'allow_bad_urls' => TRUE,
    'import_site_prefix' => 'school-of-humanities/',
    'menu_parent_id' => 'menu-school-of-humanities:0',
    'handle_duplicates' => '8',
    'handle_no_title' => '4',
    'recursion_behaviour' => 'recurse after ',
    'keep_temp_files' => TRUE,
    'import_html_preprocess_cleanups' => array(
      0 => array(
        'xpath' => '//script',
        'action' => 'remove',
      ),
      1 => array(
        'xpath' => 'parent-fieldname-title',
        'action' => 'set_aside',
      ),
    ),
    'import_html_replace_patterns' => array(
      0 => array(
        'search' => '',
        'regexp' => 0,
        'replace' => '',
      ),
    ),
  );

  // Exported setting (only the non-default values): Behrend Admissions & Financial Aid
  $import_html_profiles['Behrend Admissions & Financial Aid'] = array(
    'profile_id' => 'Behrend Admissions & Financial Aid',
    'translation_template' => FALSE,
    'content_tag_id' => 'parent-fieldname-text',
    'file_exclusions' => '^_
/_
CVS
^\\.
/\\.',
    'relink_files' => TRUE,
    'allow_bad_urls' => TRUE,
    'hide_default_menu_placeholders' => TRUE,
    'menu_parent_id' => 'menu-admissions-financial-aid:0',
    'handle_no_title' => '4',
    'import_html_preprocess_cleanups' => array(
      0 => array(
        'xpath' => '//script',
        'action' => 'remove',
      ),
      1 => array(
        'xpath' => 'parent-fieldname-title',
        'action' => 'set_aside',
      ),
    ),
    'import_html_replace_patterns' => array(
      0 => array(
        'search' => '',
        'regexp' => 0,
        'replace' => '',
      ),
    ),
  );

  // Exported setting (only the non-default values): Student Life
  $import_html_profiles['Student Life'] = array(
    'profile_id' => 'Student Life',
    'translation_template' => FALSE,
    'content_tag_id' => 'parent-fieldname-text',
    'file_exclusions' => '^_
/_
CVS
^\\.
/\\.',
    'relink_files' => TRUE,
    'allow_bad_urls' => TRUE,
    'menu_parent_id' => 'menu-student-life:0',
    'import_html_preprocess_cleanups' => array(
      0 => array(
        'xpath' => '//script',
        'action' => 'remove',
      ),
      1 => array(
        'xpath' => 'parent-fieldname-title',
        'action' => 'set_aside',
      ),
    ),
    'import_html_replace_patterns' => array(
      0 => array(
        'search' => '',
        'regexp' => 0,
        'replace' => '',
      ),
    ),
  );

  // Exported setting (only the non-default values): default
  $import_html_profiles['default'] = array(
    'translation_template' => FALSE,
  );

  return $import_html_profiles;
}
