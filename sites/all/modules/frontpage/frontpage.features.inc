<?php
/**
 * @file
 * frontpage.features.inc
 */

/**
 * Implements hook_views_api().
 */
function frontpage_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function frontpage_image_default_styles() {
  $styles = array();

  // Exported image style: front-profile
  $styles['front-profile'] = array(
    'name' => 'front-profile',
    'effects' => array(
      16 => array(
        'label' => 'Crop',
        'help' => 'Cropping will remove portions of an image to make it the specified dimensions.',
        'effect callback' => 'image_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_crop_form',
        'summary theme' => 'image_crop_summary',
        'module' => 'image',
        'name' => 'image_crop',
        'data' => array(
          'width' => '200',
          'height' => '200',
          'anchor' => 'center-top',
        ),
        'weight' => '1',
      ),
      17 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '95',
          'height' => '75',
        ),
        'weight' => '2',
      ),
    ),
  );

  // Exported image style: front_thumbnail
  $styles['front_thumbnail'] = array(
    'name' => 'front_thumbnail',
    'effects' => array(
      27 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '67',
          'height' => '67',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: responsive
  $styles['responsive'] = array(
    'name' => 'responsive',
    'effects' => array(),
  );

  // Exported image style: spotlight_desktop_980
  $styles['spotlight_desktop_980'] = array(
    'name' => 'spotlight_desktop_980',
    'effects' => array(
      25 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '1280',
          'height' => '630',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: spotlight_larger_desktop
  $styles['spotlight_larger_desktop'] = array(
    'name' => 'spotlight_larger_desktop',
    'effects' => array(
      26 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '1920',
          'height' => '500',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: spotlight_mobile_480
  $styles['spotlight_mobile_480'] = array(
    'name' => 'spotlight_mobile_480',
    'effects' => array(
      24 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '480',
          'height' => '180',
        ),
        'weight' => '2',
      ),
    ),
  );

  // Exported image style: spotlight_portait_tablet_980
  $styles['spotlight_portait_tablet_980'] = array(
    'name' => 'spotlight_portait_tablet_980',
    'effects' => array(
      23 => array(
        'label' => 'Resize',
        'help' => 'Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => '960',
          'height' => '630',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: spotlight_tablet_767
  $styles['spotlight_tablet_767'] = array(
    'name' => 'spotlight_tablet_767',
    'effects' => array(
      22 => array(
        'label' => 'Resize',
        'help' => 'Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => '768',
          'height' => '504',
        ),
        'weight' => '1',
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function frontpage_node_info() {
  $items = array(
    'article' => array(
      'name' => t('Article'),
      'base' => 'node_content',
      'description' => t('Use <em>articles</em> for time-sensitive content like news, press releases or blog posts.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
