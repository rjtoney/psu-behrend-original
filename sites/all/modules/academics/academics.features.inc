<?php
/**
 * @file
 * academics.features.inc
 */

/**
 * Implements hook_node_info().
 */
function academics_node_info() {
  $items = array(
    'academics_page' => array(
      'name' => t('Academics page'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
