<?php
/**
 * @file
 * news_event_user_import.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function news_event_user_import_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'event_csv';
  $feeds_importer->config = array(
    'name' => 'event csv',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'event',
        'expire' => '-1',
        'author' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'year',
            'target' => 'field_event_year',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'first_level',
            'target' => 'field_event_first_level',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'second_level',
            'target' => 'field_event_second_level',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'third_level',
            'target' => 'field_event_third_level',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'description',
            'target' => 'field_summary',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'location',
            'target' => 'location_event',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'location',
            'target' => 'field_event_location',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'startDate',
            'target' => 'event_starts:start',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'endDate',
            'target' => 'event_starts:end',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'eventType',
            'target' => 'field_event_og_tags',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'eventURL',
            'target' => 'field_event_url',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'contactName',
            'target' => 'field_event_contact_name',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'contactEmail',
            'target' => 'field_contact_email',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'contactPhone',
            'target' => 'field_event_contact_phone',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'eventType',
            'target' => 'field_event_page_tags',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => 0,
          ),
        ),
        'update_existing' => '0',
        'input_format' => 'filtered_html',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 1,
  );
  $export['event_csv'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'news_import';
  $feeds_importer->config = array(
    'name' => 'News import',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'news',
        'expire' => '-1',
        'author' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'Title',
            'target' => 'title',
            'unique' => 0,
          ),
          1 => array(
            'source' => 'label_image_caption',
            'target' => 'field_og_news_image_caption',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'label_description',
            'target' => 'field_news_description',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'label_body_text',
            'target' => 'body',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'label_short_name',
            'target' => 'url',
            'unique' => 0,
          ),
        ),
        'update_existing' => '0',
        'input_format' => 'filtered_html',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 1,
  );
  $export['news_import'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'user_importer_for_faculty';
  $feeds_importer->config = array(
    'name' => 'User Importer for faculty',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsUserProcessor',
      'config' => array(
        'roles' => array(
          4 => 0,
          3 => 0,
        ),
        'status' => '1',
        'defuse_mail' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'text',
            'target' => 'field_user_biography',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'school',
            'target' => 'field_user_psb_work_unit',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'phone',
            'target' => 'field_user_phone',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'proper',
            'target' => 'field_user_employee_type',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'office',
            'target' => 'field_user_office_location',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'prefix',
            'target' => 'field_user_prefix',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'suffix',
            'target' => 'field_user_suffix',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'image',
            'target' => 'field_user_image',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'job_title',
            'target' => 'field_user_psu_job_title',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'username',
            'target' => 'name',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'email',
            'target' => 'mail',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'email',
            'target' => 'field_psu_email',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'name',
            'target' => 'field_user_full_name',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => 0,
        'input_format' => 'filtered_html',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 1,
  );
  $export['user_importer_for_faculty'] = $feeds_importer;

  return $export;
}
