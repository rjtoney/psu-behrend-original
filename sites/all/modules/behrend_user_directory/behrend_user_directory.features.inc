<?php
/**
 * @file
 * behrend_user_directory.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function behrend_user_directory_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "custom_formatters" && $api == "custom_formatters") {
    return array("version" => "2");
  }
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}
