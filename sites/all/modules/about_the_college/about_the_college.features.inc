<?php
/**
 * @file
 * about_the_college.features.inc
 */

/**
 * Implements hook_node_info().
 */
function about_the_college_node_info() {
  $items = array(
    'about_page' => array(
      'name' => t('About page'),
      'base' => 'node_content',
      'description' => t('Use this content type to create a <em>page</em> in the <em>About section</em> of the site'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
