<?php
/**
 * @file
 * behrend_frontpage_slide.features.inc
 */

/**
 * Implements hook_node_info().
 */
function behrend_frontpage_slide_node_info() {
  $items = array(
    'frontpage_slide' => array(
      'name' => t('Frontpage Slide'),
      'base' => 'node_content',
      'description' => t('Use this content type to create rotational images for the Frontpage Slide'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
