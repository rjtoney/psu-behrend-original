<?php
/**
 * @file
 * school_of_humanities.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function school_of_humanities_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-school-of-humanities
  $menus['menu-school-of-humanities'] = array(
    'menu_name' => 'menu-school-of-humanities',
    'title' => 'School of Humanities',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('School of Humanities');


  return $menus;
}
