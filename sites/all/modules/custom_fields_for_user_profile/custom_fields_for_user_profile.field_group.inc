<?php
/**
 * @file
 * custom_fields_for_user_profile.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function custom_fields_for_user_profile_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_biography|user|user|form';
  $field_group->group_name = 'group_biography';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_standard_pages';
  $field_group->data = array(
    'label' => 'Biography',
    'weight' => '10',
    'children' => array(
      0 => 'field_user_biography',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_biography|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_outreach|user|user|form';
  $field_group->group_name = 'group_outreach';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_standard_pages';
  $field_group->data = array(
    'label' => 'Outreach',
    'weight' => '31',
    'children' => array(
      0 => 'field_user_outreach',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Outreach',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_outreach|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_psb_profile_card|user|user|default';
  $field_group->group_name = 'group_psb_profile_card';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Penn State Behrend Profile Card',
    'weight' => '0',
    'children' => array(
      0 => 'field_first_name',
      1 => 'field_last_name',
      2 => 'field_suffix',
      3 => 'field_prefix',
      4 => 'field_email',
      5 => 'field_office_ext',
      6 => 'field_job_title',
      7 => 'field_work_unit',
      8 => 'field_user_image',
      9 => 'field_office_location',
      10 => 'picture',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_psb_profile_card|user|user|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_psb_profile_card|user|user|form';
  $field_group->group_name = 'group_psb_profile_card';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Penn State Behrend Profile Card',
    'weight' => '0',
    'children' => array(
      0 => 'field_user_image',
      1 => 'field_user_prefix',
      2 => 'field_user_first_name',
      3 => 'field_user_last_name',
      4 => 'field_user_suffix',
      5 => 'field_user_psu_job_title',
      6 => 'field_user_psb_work_unit',
      7 => 'field_user_office_location',
      8 => 'field_user_phone',
      9 => 'field_psu_email',
      10 => 'picture',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_psb_profile_card|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_publications|user|user|form';
  $field_group->group_name = 'group_publications';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_standard_pages';
  $field_group->data = array(
    'label' => 'Publications',
    'weight' => '14',
    'children' => array(
      0 => 'field_user_publications',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_publications|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_research|user|user|form';
  $field_group->group_name = 'group_research';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_standard_pages';
  $field_group->data = array(
    'label' => 'Research',
    'weight' => '12',
    'children' => array(
      0 => 'field_user_research',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_research|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_service_page|user|user|form';
  $field_group->group_name = 'group_service_page';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_standard_pages';
  $field_group->data = array(
    'label' => 'Service',
    'weight' => '13',
    'children' => array(
      0 => 'field_user_service',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_service_page|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_standard_pages|user|user|form';
  $field_group->group_name = 'group_standard_pages';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Faculty Standard Information',
    'weight' => '7',
    'children' => array(
      0 => 'group_biography',
      1 => 'group_outreach',
      2 => 'group_publications',
      3 => 'group_research',
      4 => 'group_service_page',
      5 => 'group_teaching',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_standard_pages|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_teaching|user|user|form';
  $field_group->group_name = 'group_teaching';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_standard_pages';
  $field_group->data = array(
    'label' => 'Teaching',
    'weight' => '11',
    'children' => array(
      0 => 'field_user_teaching',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_teaching|user|user|form'] = $field_group;

  return $export;
}
