<?php
/**
 * @file
 * custom_fields_for_user_profile.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function custom_fields_for_user_profile_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'user_importer_for_faculty';
  $feeds_importer->config = array(
    'name' => 'User Importer for faculty',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsUserProcessor',
      'config' => array(
        'roles' => array(
          4 => 0,
          3 => 0,
        ),
        'status' => '1',
        'defuse_mail' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'text',
            'target' => 'field_user_biography',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'school',
            'target' => 'field_user_psb_work_unit',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'phone',
            'target' => 'field_user_phone',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'proper',
            'target' => 'field_user_employee_type',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'office',
            'target' => 'field_user_office_location',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'prefix',
            'target' => 'field_user_prefix',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'suffix',
            'target' => 'field_user_suffix',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'image',
            'target' => 'field_user_image',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'job_title',
            'target' => 'field_user_psu_job_title',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'username',
            'target' => 'name',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'email',
            'target' => 'mail',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'email',
            'target' => 'field_psu_email',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'name',
            'target' => 'field_user_full_name',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => 0,
        'input_format' => 'filtered_html',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 1,
  );
  $export['user_importer_for_faculty'] = $feeds_importer;

  return $export;
}
