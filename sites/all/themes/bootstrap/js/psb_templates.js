﻿
CKEDITOR.addTemplates( '2 Column',
{
  // The name of the subfolder that contains the preview images of the templates.
  imagesPath : CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),

  // Template definitions.
  templates :
    [
      {
        title: '2 Column Layout (Bootstrap3)',
        image: 'template1.gif',
        description: 'Fluid layout based on bootstrap Classes.',
        html:
          '<div class="span4"><h2>Column 1</h2>' +
          '<p><br /></p>' +
          '</div>' +
          '<div class="span3"><h2>Column 2</h2>' +
          '<p><br /></p>' +
          '</div>'
      }
    ]
});
