<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<div class="row" id="search" style="background-color:#000;border-bottom:2px solid #50545E;">
  <div class="container"><div class="col-md-4 pull-right"><?php if (!empty($page['search'])): ?>
            <?php print render($page['search']); ?>
          <?php endif; ?></div></div></div>
<div role="banner" class="clearfix" id="header" style="background-color:#262c3a;">

  <div id="behrend" class="container">
    <div id="brand" class="row">
      <div id="logos" class="col-md-2 col-xs-2 col-sm-2 col-lg-2">
        <!-- Logo -->
         <a rel="home" title="Penn State Erie, The Behrend College" href="/" id="logo-image">   <img src="//dev03.bd.psu.edu/sites/all/themes/bootstrap/logo.png" alt="Penn State University" style="max-height:75px;" id="logo-shield"></a>  </div>
              <div id="slogan" class="col-md-5 col-xs-6 sol-sm-6"> <?php print render($page['slogan']); ?>
              <!--<img id="wordmark" style="max-height:36px;" alt="Penn State Erie, The Behrend College"src="//dev03.bd.psu.edu/sites/all/themes/bootstrap/2033.png">

        <! /logo -->
      </div>

  </div>
  </div>
</div>
<div style="background: #2c76c7;" role="banner" class="clearfix" id="mainmenu">
<div class="container" id="menu">
<div style="border:0px !important;border-radius:0 !important;" role="navigation" class="navbar navbar-default">

      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
      <button data-target="#navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
        <span class="sr-only">Toggle navigation</span>

        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>


          <div id="navbar-collapse" class="navbar-collapse collapse bs-navbar-collapse nav-justified">



     <div role="navigation">
                      <ul class="menu nav navbar-nav"><li class="first leaf"><a id="Home" title="Greener Behrend Home" href="/school-of-science/research-outreach/greener-behrend/home">Home</a></li>
<li class="collapsed"><a id="admissions" title="Goals of Greener Behrend" href="/school-of-science/research-outreach/greener-behrend/goals">Goals</a></li>
<li class="leaf"><a id="academics" title="Learn about Ways to Help" href="/school-of-science/research-outreach/greener-behrend/how-can-i-help">How Can I Help?</a></li>
<li class="leaf"><a id="student-life" title="Arboretum at Penn State Behrend" href="/student-life">Arboretum</a></li>
<!--<li class="leaf"><a id="outreach" href="http://www.psblions.com/landing/index">Athletics</a></li>
<li class="last leaf"><a id="outreach" title="provide innovative, integrated, and high-quality teaching, research, and outreach programs, with a particular focus on serving the tristate region’s economic, social, and intellectual development." href="/research-outreach">Outreach</a></li> -->
</ul>                               <div class="pull-right hidden-xs">
<ul style="" id="social">
  <li style="display:inline;"><img src="//dev03.bd.psu.edu/sites/all/themes/bootstrap/facebook.png"></li>
  <li style="display:inline;"><img src="//dev03.bd.psu.edu/sites/all/themes/bootstrap/twitter.png"></li>
  <li style="display:inline;"><img src="//dev03.bd.psu.edu/sites/all/themes/bootstrap/camera.png"></li>
  <li style="display:inline;"><img src="//dev03.bd.psu.edu/sites/all/themes/bootstrap/youtube.png"></li>
</ul>

  </div>

    </div>
      </div>
      </div>
</div></div>

<!--<div class="alert alert-info" role="alert"><a href="https://twitter.com/psbehrend"><span role="img" aria-label="facebook logo" class="fa fa-twitter-square"></span></a>
<a href="//www.youtube.com/PennStateBehrend1"><span role="img" aria-label="facebook logo" class="fa fa-youtube-square"></span></a>

      <strong>Web Accessibility Enhancements</strong><hr> This is the development website for psbehrend.psu.edu; this is a live site and will be changing during the Accessibility Enhancements project. This is a <strong>work in progress; for a behind-the-scenes look</strong> , <a href="//dev03.bd.psu.edu/node/9043" class="alert-link">You may click here to view proposed designs</a>.
    </div>  <h1 class="invisible">Homepage</h1>
    <header role="banner" id="page-header">
        <p class="lead"></p>
    </header>-->

<div class="container" style="">

    <!-- /#header -->
    <div id="main-content" class="row"><a name="main-content"></a>
    </div>
    <div class="row" style="background-color:#FFF; margin-top: 10px;padding-top: 10px;margin-bottom:10px;padding-bottom:10px;">
        <div class="col-md-10"><?php print render($page['carousel']); ?></div>
        <div class="col-md-2">

  <img src="//dev03.bd.psu.edu/sites/all/themes/psb_tb3/lion.jpg" alt="Penn State Lion" class="hidden-sm hidden-xs Lion" style="padding-bottom:9px;margin-top:5px;width:100%;"/></a>
            <ul
            class="nav nav-tabs nav-stacked" id="frontnav" style="margin-top:">
            <li style="border-top: 2px solid #ffffff;"><a href="//psbehrend.psu.edu">Penn State Behrend</a>
                <li style="border-top: 2px solid #ffffff;"><a href="//psbehrend.psu.edu/school-of-science">School of Science</a>
                </li>
                <li style="border-top: 2px solid #ffffff;"><a href="//psbehrend.psu.edu/school-of-science/research-outreach/greener-behrend/faculty-staff">Outreach Center Faculty</a>
                </li> <!--
                <li style="border-top: 2px solid #ffffff;"><a href="alumni">Alumni &amp; Friends</a>
                </li>
                <li style="border-top: 2px solid #ffffff;"><a href="faculty-staff">Faculty &amp; Staff</a>
                </li> -->  <img style="margin-top:10px" width="100%" src="//dev03.bd.psu.edu/sites/all/themes/psb_tb3/penn_state_lives_here.jpg" alt="Penn State Lion" class="hidden-sm hidden-xs Lion" style="width:100%;height:auto;"/></a>

                                <li>
                </li>
                </ul>
                <!--<div class="col-md-3" style="margin-left: 0px !important; background-color: #0066CC !important; padding-top: 5px !important; padding-bottom: 5px !important;"> <a href="https://www.facebook.com/pennstatebehrend"><img src="/sites/all/themes/psb/images/facebook_32.png" style="margin-left: 1em;" alt="Link to Penn State Behrend's Facebook Page" class="img-responsive" /></a>
                    <a
                    href="https://twitter.com/psbehrend">
                        <img src="/sites/all/themes/psb/images/twitter_32.png" style="margin-left: 1em;"
                        alt="Link to Penn State Behrend's Twitter Page" class="img-responsive"
                        />
                        </a> <a href="//www.youtube.com/PennStateBehrend1"><img src="/sites/all/themes/psb/images/youtube_32.png" style="margin-left: 1em;" alt="Link to Penn State Behrend's YouTube Channel" class="img-responsive" /></a>
                        <a
                        href="//instagram.com/psbehrend">
                            <img src="/sites/all/themes/psb/images/instagram.png" style="margin-left: 1em;"
                            alt="View Penn State Behrend on Instagram" class="img-responsive" />
                            </a>
                </div>-->
        </div>
    </div>
</div>
        <div class="container" style="padding-left: 0 !important;padding-right: 0 !important;margin-bottom:10px;">
<div class="row">
        <div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div property="content:encoded" class="field-item even"><h1><span style="color:#000000;"><strong><span style="font-size: 16px;"><span style="font-family: arial,helvetica,sans-serif;">Ecological Commitment Statement test</span></span></strong></span></h1>
<h2 class="rteindent1"><span style="color:#000000;"><strong><span style="font-size: 16px;"><span style="font-family: arial,helvetica,sans-serif;">Vision</span></span></strong></span></h2>
<div class="rteindent2"><span style="color:#000000;">Penn State Erie, The Behrend College, will have a campus community that is educated about and committed to establishing and maintaining a responsible relationship with the fixed and finite natural resources on which we depend.</span></div>
<div class="rteindent1">&nbsp;</div>
<h2 class="rteindent1"><span style="color:#000000;"><span style="font-size: 16px;"><span style="font-family: arial,helvetica,sans-serif;"><strong>Mission</strong></span></span></span></h2>
<div class="rteindent2"><span style="color:#000000;">To develop and implement strategies and related programs for campus practices and policies that further ecological sustainability within the college community.</span></div>
<div class="rteindent1">&nbsp;</div>
<h2 class="rteindent1"><span style="color:#000000;"><strong><span style="font-size: 16px;"><span style="font-family: arial,helvetica,sans-serif;">Beliefs</span></span></strong></span></h2>
<div class="rteindent2"><span style="color:#000000;">Members of the college community recognize that we live in an environment where natural resources are plentiful, yet finite. Recognizing that higher education is not values neutral, we believe that institutions have a responsibility to identify and promote responsibility and citizenship among all constituents and constituent groups. Penn State’s land-grant history and its mission to create and disseminate knowledge by being a world-class leader in research, education, and service underscore this belief.</span></div>
<div class="rteindent1">&nbsp;</div>
<h1><span style="color:#000000;"><strong><span style="font-size: 15px;"><span style="font-family: arial,helvetica,sans-serif;">Consequently, we believe that we have a responsibility to foster and build a respect for the environment, including:</span></span></strong></span></h1>
<ul><li>
<p><span style="color:#000000;">appreciating the place of humans and institutions within their surroundings</span></p>
</li>
<li>
<p><span style="color:#000000;">committing to live within our environmental limits and consider the full cost of our endeavors</span></p>
</li>
<li>
<p><span style="color:#000000;">assuming a leadership role in communicating and teaching civic responsibility.&nbsp;</span></p>
</li>
</ul><div>&nbsp;</div>
</div></div></div>
  </div>
       <!-- <div class="row visible-md">
            <div class="col-md-12 btn-group" style="text-align: center;"> <a class="btn btn-primary" href="//behrend.psu.edu/Events/MasterCalendar.aspx"
                target="_blank">Events Calendar</a>  <a class="btn btn-primary" href="//behrend.psu.edu/VirtualTour/PSBVirtualTour.html"
                target="_blank" title="Take the Penn State Behrend Virtual Tour">Virtual Tour</a>
                <a
                href="//behrendblog.wordpress.com/ " target="_blank" class="btn btn-primary"
                title="Read the Penn State Behrend Blog">Blog</a> <a class="btn btn-primary" href="/webcams" target="_blank" title="View the Penn State Behrend Web Cams">Webcams</a>
                    <a
                    href="/admissions-financial-aid/visit-behrend" class="btn btn-primary"
                    title="Visit Us">Visit</a> <a href="/admissions-financial-aid/apply" class="btn btn-primary">Apply</a>
            </div>
        </div>
        <div class="row visible-sm">
            <div class="col-md-6 btn-group" style="text-align: center;"> <a class="btn btn-primary" href="//behrend.psu.edu/Events/MasterCalendar.aspx"
                target="_blank">Events Calendar</a>  <a href="//behrendblog.wordpress.com/ "
                target="_blank" class="btn btn-primary" title="Read the Penn State Behrend Blog">Blog</a>
                <a
                class="btn btn-primary" href="/webcams" target="_blank" title="View the Penn State Behrend Web Cams">Webcams</a> <a href="/admissions-financial-aid/visit-behrend" class="btn btn-primary"
                    title="Visit Us">Visit</a>
 <a href="/admissions-financial-aid/apply" class="btn btn-primary">Apply</a>
            </div>
        </div> -->
        <footer class="footer container"></footer>
    </div>
</div>
<div role="contentinfo" class="clearfix footer" id="footer" style="background-color:#262c3a;">
  <div class="container">
    <div class="row" id="footer-content">
      <div class="col-xs-12 col-sm-1-4 col-md-2">
        <div class="block">
          <h3>Schools</h3>
          <ul class="footer-list">
            <li><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/school-of-business">School of Business</a></li>
            <li><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/school-of-engineering">School of Engineering</a></li>
            <li><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/school-of-humanities-social-sciences">School of Humanities</a></li>
            <li><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/school-of-science">School of Science</a></li>
            </ul>
        </div>
      </div>
      <div class="col-xs-12 col-sm-1-4 col-md-2">
        <div class="block">
          <h3>Research</h3>
          <ul class="footer-list">
            <li><a href="//psbehrend.psu.edu/research-outreach">Research &amp Outreach</a></li>
            <li><a href="//psbehrend.psu.edu/research-outreach/student-research">Student Researcs</a></li>
            <li><a href="//psbehrend.psu.edu/research-outreach/faculty-research/faculty-research">Faculty Research</a></li>
            <li><a href="//psbehrend.psu.edu/research-outreach/outreach-initiatives">Outreach Initiatives</a></li>
          </ul>
          <h3>Student Life</h3>
          <ul class="footer-list">
            <li><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/student-life/student-activities-1">Student Activities</a></li>
            <li><a data-ua-action="//psbehrend.psu.edu/student-life/student-services" data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/student-life/student-services">Student Services</a></li>
            <li class="last"><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/student-life/housing-and-residence-life">Housing &amp Residence Life</a></li>
          </ul>
        </div>
      </div>
      <div class="col-xs-12 col-sm-1-4 col-md-2">
        <div class="block">
          <h3>Academic Services</h3>
          <ul class="footer-list">
            <li><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/Academics/academic-services/acpc">Academics &amp; Career Planning Center</a></li>
            <li><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/Academics/academic-services/lrc">Learning Resource Center (Tutoring)</a></li>
      <li><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/Academics/academic-services/college-registrar">College Register</a></li>
            <li><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/Academics/academic-services/bursar">Bursar's Office</a></li>
      <li><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/Academics/academic-services/adult">Center for Adult Students</a></li>

      </ul>
          <!-- <h3>Online Learning</h3>
          <ul class="footer-list">
            <li><a data-ua-label="footer" class="su-link" href="//online.stanford.edu">Stanford Online</a></li>
          </ul> -->
        </div>
      </div>
      <div class="col-xs-12 col-sm-1-4 col-md-2">
        <div class="block">
          <h3>About Behrend</h3>
          <ul class="footer-list">
            <li><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/about-the-college/at-a-glance">At a Glance</a></li>
            <li><a href="//psbehrend.psu.edu/about-the-college/collegehistory-1">History</a></li>
           <li><a href="//psbehrend.psu.edu/about-the-college/collegeleadership-1">College Leadership</a></li>
             <li><a href="//psbehrend.psu.edu/about-the-college/people-and-departments">People &amp Departments</a></li>
          </ul>
        </div>
        <div class="block">
          <h3>Admissions</h3>
          <ul class="footer-list">
            <li><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/admissions-financial-aid/undergraduate-admissions">Undergraduate</a></li>
            <li><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/admissions-financial-aid/graduate-admissions">Graduate</a></li>
            <li><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/admissions-financial-aid/adult-admissions">Adult Admissions</a></li>


          </ul>
        </div>
      </div>
      <div class="col-xs-12 col-sm-1-4 col-md-2">

      </div>
      <div class="col-xs-12 col-md-2" id="hp-buttons">
        <ul>
                    <li><a data-ua-action="visitors-gateway" data-ua-label="footer" class="btn btn-primary btn-u btn-block su-link" href="//psbehrend.psu.edu/admissions-financial-aid/visit-behrend">Visit</a> </li>
          <li><a class="btn btn-primary btn-u btn-block" href="//psbehrend.psu.edu/admissions-financial-aid/apply"> Apply</a> </li>

          <li><a data-ua-label="footer" class="btn btn-primary btn-u btn-block su-link" href="//psbehrend.psu.edu/webcams"> Getting Here</a> </li>
          <li><a data-ua-label="footer" class="btn btn-primary btn-u btn-block su-link" href="//behrendblog.wordpress.com/"> Blog</a> </li>
          <li><a class="btn btn-primary btn-u btn-block" href="/contact"> Webcams</a> </li>
        </ul>
      </div>
    </div>
  </div>

  <!-- Global footer snippet start -->
  <div id="global-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-7 pull-left" id="bottom-logo"> <p>© Penn State Erie, The Behrend College.  4701 College Drive Erie, PA 16563 | 814-898-6000 | Toll-free: 866-374-3378 <br /><p style="margin-top:5px;">Web Contact: bdwebmaster@psu.edu | Site Map | Login</p></div>
        <!-- #bottom-logo end -->
        <div class="col-md-5 pull-right" id="bottom-text">
          <ul>
            <li class="maps alt"><a data-ua-label="global-footer" data-ua-action="//psbehrend.psu.edu/accessibility" class="su-link" href="//psbehrend.psu.edu/accessibility">Accessibility</a></li>
            <li class="search-stanford"><a href="//www.psu.edu/ur/copyright.html">Copyright</a></li>
            <li class="terms alt"><a href="//www.psu.edu/ur/legal.html">Privacy &amp Legal Statements</a></li>
            <li class="emergency-info"><a data-ua-label="global-footer" class="su-link" href="//www.psu.edu/hotlines">Hotlines</a></li>
          </ul>
         <p class="pull-right">The Pennsylvania State University 2014</p>
        </div>
        <!-- .bottom-text end -->
        <div class="clear"></div>
        </div>
      <!-- .row end -->
    </div>
    <!-- .container end -->
  </div>
  <!-- global-footer end -->
  <!-- Global footer snippet end -->

</div>
