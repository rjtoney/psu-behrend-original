<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<div class="row" id="search" style="background-color:#000;border-bottom:2px solid #50545E;">
  <div class="container"><div class="col-md-4 pull-right"><?php if (!empty($page['search'])): ?>
            <?php print render($page['search']); ?>
          <?php endif; ?></div></div></div>
<div role="banner" class="clearfix" id="header" style="background-color:#262C39;">

  <div class="container">
    <div id="brand" class="row">
      <div class="col-xs-2">
        <!-- Logo -->
         <a rel="home" title="Center for eLearning Initiatives at Penn State Behrend" href="/cei/" id="logo-image">
              <img src="http://www.psu.edu/profiles/psu_profile/themes/psu_main/logo.png" alt="Penn State University" id="logo-shield"></a> </div>
              <div class="col-xs-6">
              <img id="wordmark" alt="Penn State Erie, The Behrend College" style="padding-left:40px;" src="http://behrend-elearn.psu.edu/cei/sites/default/files/logo2.png">

        <!-- /logo -->
      </div>
    <div class="col-md-3 pull-right">


  </div>
  </div>
  </div>
</div>
<div id="mainmenu" class="clearfix" role="banner" style="background:transparent;">
<div id="menu" class="container">
<div class="navbar navbar-default" role="navigation" style="border:0px !important;border-radius:0 !important;">

      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>


    <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
      <div class="navbar-collapse collapse bs-navbar-collapse" id="navbar-collapse">



     <div role="navigation">
          <?php if (!empty($primary_nav)): ?>
            <?php print render($primary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($secondary_nav)): ?>
            <?php print render($secondary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($page['navigation'])): ?>
            <?php print render($page['navigation']); ?>
          <?php endif; ?>
 <div class="navbar-text pull-right visible-lg"><a title="Facebook" href="https://www.facebook.com/pennstatebehrend">FB</a>
  <a title="Twitter" href="https://www.facebook.com/pennstatebehrend">TWITTER</a>
  <a title="Instagram" href="https://www.facebook.com/pennstatebehrend">INSTAGRAM</a>
  <a title="Youtube" href="https://www.facebook.com/pennstatebehrend">YOUTUBE</a>
</div>
        </nav>
    </div>
      </div>
    <?php endif; ?>
  </div>
</div></div>

<!--<div class="alert alert-info" role="alert">
      <strong>Web Accessibility Enhancements</strong><hr> This is the development website for psbehrend.psu.edu; this is a live site and will be changing during the Accessibility Enhancements project. This is a <strong>work in progress; for a behind-the-scenes look</strong> , <a href="http://dev03.bd.psu.edu/node/9043" class="alert-link">You may click here to view proposed designs</a>.
    </div> !-->

  <div class="container">

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>

    <section<?php print $content_column_class; ?>>
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if (!empty($title)): ?>
        <h1 class="page-header"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>

      <?php
$account = menu_get_object('user');
global $user;
$mail = $user->mail;
// Switch to the external DB
db_set_active('mydb');
    $sql = "SELECT * FROM ViewCombos WHERE Email = '$user->mail'";
    $result = db_query($sql)->fetchField();
  if ($result) {
    echo '<h2>Your Mailbox Number is:</h2>';
  print $result;
  db_set_active();
  } else {
  echo '<h2>You currently do not have a mailbox</h2>';
  db_set_active();
  }
?>
    </section>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>

  </div>
</div>
<div role="contentinfo" class="clearfix footer" id="footer">
  <div class="container">
    <div class="row" id="footer-content">
      <div class="col-xs-12 col-sm-1-4 col-md-2">
        <div class="block">
          <h3>Schools</h3>
          <ul class="footer-list">
            <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/school-of-business">School of Business</a></li>
            <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/school-of-engineering">School of Engineering</a></li>
            <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/school-of-humanities-social-sciences">School of Humanities</a></li>
            <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/school-of-science">School of Science</a></li>
            </ul>
        </div>
      </div>
      <div class="col-xs-12 col-sm-1-4 col-md-2">
        <div class="block">
          <h3>Research</h3>
          <ul class="footer-list">
            <li><a href="http://psbehrend.psu.edu/research-outreach">Research &amp Outreach</a></li>
            <li><a href="http://psbehrend.psu.edu/research-outreach/student-research">Student Researcs</a></li>
            <li><a href="http://psbehrend.psu.edu/research-outreach/faculty-research/faculty-research">Faculty Research</a></li>
            <li><a href="http://psbehrend.psu.edu/research-outreach/outreach-initiatives">Outreach Initiatives</a></li>
          </ul>
          <h3>Student Life</h3>
          <ul class="footer-list">
            <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/student-life/student-activities-1">Student Activities</a></li>
            <li><a data-ua-action="http://psbehrend.psu.edu/student-life/student-services" data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/student-life/student-services">Student Services</a></li>
            <li class="last"><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/student-life/housing-and-residence-life">Housing &amp Residence Life</a></li>
          </ul>
        </div>
      </div>
      <div class="col-xs-12 col-sm-1-4 col-md-2">
        <div class="block">
          <h3>Academic Services</h3>
          <ul class="footer-list">
            <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/Academics/academic-services/acpc">Academics &amp; Career Planning Center</a></li>
            <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/Academics/academic-services/lrc">Learning Resource Center (Tutoring)</a></li>
      <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/Academics/academic-services/college-registrar">College Register</a></li>
            <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/Academics/academic-services/bursar">Bursar</a></li>
      <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/Academics/academic-services/adult">Center for Adult Students</a></li>
            <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/Academics/academic-services/lrc">Bursar</a></li>

      </ul>
          <!-- <h3>Online Learning</h3>
          <ul class="footer-list">
            <li><a data-ua-label="footer" class="su-link" href="http://online.stanford.edu">Stanford Online</a></li>
          </ul> -->
        </div>
      </div>
      <div class="col-xs-12 col-sm-1-4 col-md-2">
        <div class="block">
          <h3>About Behrend</h3>
          <ul class="footer-list">
            <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/about-the-college/at-a-glance">At a Glance</a></li>
            <li><a href="http://psbehrend.psu.edu/about-the-college/collegehistory-1">History</a></li>
           <li><a href="http://psbehrend.psu.edu/about-the-college/collegeleadership-1">College Leadership</a></li>
             <li><a href="http://psbehrend.psu.edu/about-the-college/people-and-departments">People &amp Departments</a></li>
          </ul>
        </div>
        <div class="block">
          <h3>Admissions</h3>
          <ul class="footer-list">
            <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/admissions-financial-aid/undergraduate-admissions">Undergraduate</a></li>
            <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/admissions-financial-aid/graduate-admissions">Graduate</a></li>
            <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/admissions-financial-aid/adult-admissions">Adult Admissions</a></li>


          </ul>
        </div>
      </div>
      <div class="col-xs-12 col-sm-1-4 col-md-2">
        <div class="block">
          <h3>Resources</h3>
          <ul class="footer-list">
            <li><a href="/atoz/">A - Z Index</a></li>
            <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/sites/default/files/admissions-financial-aid/visit-behrend">Campus Map</a></li>
            <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/student-life/student-services/police/parking">Parking</a></li>
            <li><a data-ua-label="footer" class="su-link" href="http://psbehrend.psu.edu/about-the-college/about-erie/getting-here-directions">Getting Here</a></li>
          </ul>
        </div>
      </div>
      <div class="col-xs-12 col-md-2" id="hp-buttons">
        <ul>
          <li><a class="btn btn-primary btn-u btn-block" href="http://psbehrend.psu.edu/admissions-financial-aid/apply"><i class="fa fa-lightbulb-o fa-fw"></i> <span>Apply</span> </a> </li>
          <li><a data-ua-action="visitors-gateway" data-ua-label="footer" class="btn btn-primary btn-u btn-block su-link" href="http://psbehrend.psu.edu/admissions-financial-aid/visit-behrend"><i class="fa fa-plane fa-fw"></i> <span>Visit Campus</span></a> </li>
          <li><a data-ua-label="footer" class="btn btn-primary btn-u btn-block su-link" href="http://psbehrend.psu.edu/webcams"><i class="fa fa-gift fa-fw"></i> <span>Webcams</span></a> </li>
          <li><a data-ua-label="footer" class="btn btn-primary btn-u btn-block su-link" href="http://behrendblog.wordpress.com/"><i class="fa fa-user fa-fw"></i> <span>Blog</span> </a> </li>
          <li><a class="btn btn-primary btn-u btn-block" href="/contact"><i class="fa fa-comment-o fa-fw"></i> <span>Contact Us</span></a> </li>
        </ul>
      </div>
    </div>
  </div>

  <!-- Global footer snippet start -->
  <div id="global-footer">
    <div class="container">
      <div class="row">
        <div class="col-xs-6 col-sm-2" id="bottom-logo"> <a href="http://psbehrend.psu.edu"> <img alt="Penn State Erie, The Behrend College" src="http://dev03.bd.psu.edu/sites/all/themes/psb_tb3/psb-white.png"> </a> </div>
        <!-- #bottom-logo end -->
        <div class="col-xs-6 col-sm-10" id="bottom-text">
          <ul>
            <li class="home"><a href="http://psbehrend.psu.edu/sitemap">Site Map</a></li>
            <li class="maps alt"><a data-ua-label="global-footer" data-ua-action="http://psbehrend.psu.edu/accessibility" class="su-link" href="http://psbehrend.psu.edu/accessibility">Accessibility</a></li>
            <li class="search-stanford"><a href="http://www.psu.edu/ur/copyright.html">Copyright</a></li>
            <li class="terms alt"><a href="http://www.psu.edu/ur/legal.html">Privacy &amp Legal Statements</a></li>
            <li class="emergency-info"><a data-ua-label="global-footer" class="su-link" href="http://www.psu.edu/hotlines">Hotlines</a></li>
          </ul>
        </div>
        <!-- .bottom-text end -->
        <div class="clear"></div>
        <p class="copyright vcard col-sm-10 pull-right">&copy; <span class="fn org">Penn State Erie, The Behrend College</span>.&nbsp; <span class="adr">4701 College Drive</span> <span class="locality">Erie</span>, <span class="region">PA</span> <span class="postal-code">16563</span></span></p>
      </div>
      <!-- .row end -->
    </div>
    <!-- .container end -->
  </div>
  <!-- global-footer end -->
  <!-- Global footer snippet end -->

</div>

