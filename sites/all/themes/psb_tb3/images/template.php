<?php
/**
* This snippet loads a custom page-blog.tpl.php layout file and
* overrides the logo and site name when users
* are viewing blogs.
*function psb_tb3_phptemplate_variables($hook, $variables = array()) {
*  switch ($hook) {
*    case 'outreach_center_page':
*      if ((arg(2) == 'greener-behrend')) {
*        $variables['template_file'] = 'page-gb'; // loads the custom page-blog.tpl.php file
*        $variables['site_name'] = 'Greener Behrend';  // change the site name
*        $variables['logo'] = '/path/to/newlogo/logo.png'; // change the site logo
*        $variables['primary_nav'] = FALSE;
*    if ($menu_tree = menu_tree(variable_get('menu_main_links_source', 'greener-behrend'))) {
*      $vars['primary_nav'] = render($menu_tree);
*    }
*      }
*      break;
*  }
*  return $variables;
*}
*?>*/

