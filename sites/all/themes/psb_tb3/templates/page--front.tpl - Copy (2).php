<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?><div class="row hidden-print" id="search" style="background-color:#000;border-bottom:2px solid #50545E;">
  <div class="container"><div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pull-right"><?php if (!empty($page['search'])): ?>
            <?php print render($page['search']); ?>
          <?php endif; ?></div></div></div>
<div role="banner" class="clearfix" id="header" style="background-color:#262c3a;">

  <div id="behrend" class="container">
    <div id="brand" class="row"><img src="/sites/all/themes/psb_tb3/PennStateLogo-PSB-wordmark-print.png" style="width:620px;height:72px;" class="visible-print-block">

      <div id="logos" class="col-md-1 col-xs-1 col-sm-1 col-lg-1 hidden-print">
<img src="/sites/all/themes/psb_tb3/psu-logo-PSB-wordmark.png" alt="" usemap="#Map" />
<map name="Map" id="Map">
    <area alt="Penn State" title="Penn State" href="http://www.psu.edu" shape="poly" coords="2,4,75,0,74,77,1,79" />
    <area alt="Penn State Behrend" title="Penn State Behrend" href="http://psbehrend.psu.edu" shape="poly" coords="84,2,259,2,261,76,88,76" />

</map>
        <!-- /logo -->
      </div>

  </div>
  </div>
</div>
<div id="mainmenu" class="clearfix" role="banner" style="background: #2c76c7;">
<div id="menu" class="container">
<div class="navbar navbar-default" role="navigation" style="border:0px !important;border-radius:0 !important;">

      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
        <span class="sr-only">Toggle navigation</span>

<p style="front-size:1em;margin-bottom:0px;">MENU</p>
      </button>


    <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
      <div class="navbar-collapse collapse bs-navbar-collapse nav-justified" id="navbar-collapse">



     <div role="navigation">
          <?php if (!empty($primary_nav)): ?>
            <?php print render($primary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($secondary_nav)): ?>
            <?php print render($secondary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($page['navigation'])): ?>
            <?php print render($page['navigation']); ?>
          <?php endif; ?>
 <div class="pull-right visible-lg">
<ul id="social" style="">
  <li style="display:inline;"><a href="https://www.facebook.com/pennstatebehrend" target="_blank"><img alt="Link to Penn State Behrend's Facebook Page" src="<?php echo drupal_get_path('theme', 'psb_tb3'); ?>/images/facebook.png"></a></li>
  <li style="display:inline;"><a href="https://twitter.com/psbehrend" target="_blank"><img alt="Link to Penn State Behrend's Twitter Page" src="<?php echo drupal_get_path('theme', 'psb_tb3'); ?>/images/twitter.png"></a></li>
  <li style="display:inline;"><a href="http://instagram.com/psbehrend" target="_blank"><img alt="Link to Penn State Behrend Instagram" src="<?php echo drupal_get_path('theme', 'psb_tb3'); ?>/images/camera.png"></a></li>
  <li style="display:inline;"><a href="http://www.youtube.com/PennStateBehrend1" target="_blank"><img alt="Link to Penn State Behrend's Youtube" src="<?php echo drupal_get_path('theme', 'psb_tb3'); ?>/images/youtube.png"></a></li>
</ul>

  </div>
        </nav>
    </div>
      </div>
    <?php endif; ?>
  </div>
</div></div>
          <?php if (!empty($page['alert'])): ?>
            <div class="container"><div class="" role="alert">
      <?php print render($page['alert']); ?>
    </div></div>
          <?php endif; ?>
<!--<div class="alert alert-info" role="alert"><a href="https://twitter.com/psbehrend"><span role="img" aria-label="facebook logo" class="fa fa-twitter-square"></span></a>
<a href="//www.youtube.com/PennStateBehrend1"><span role="img" aria-label="facebook logo" class="fa fa-youtube-square"></span></a>

      <strong>Web Accessibility Enhancements</strong><hr> This is the development website for psbehrend.psu.edu; this is a live site and will be changing during the Accessibility Enhancements project. This is a <strong>work in progress; for a behind-the-scenes look</strong> , <a href="//dev03.bd.psu.edu/node/9043" class="alert-link">You may click here to view proposed designs</a>.
    </div>  <h1 class="invisible">Homepage</h1>
    <header role="banner" id="page-header">
        <p class="lead"></p>
    </header>-->

<div class="container" id="maincontent">

    <!-- /#header -->
    <div id="main-content" class="row"><a name="main-content"></a>
    </div>
    <div class="row" style="background-color:#FFF; margin-top: 10px;padding-top: 10px;margin-bottom:14px;padding-bottom:10px;">
        <div class="col-md-9"><?php print render($page['carousel']); ?></div>
        <div class="col-md-3">

 <a href="http://behrend.psu.edu/Events/MasterCalendar.aspx" target="_blank" alt="Visiting the Penn State Behrend Events Calendar"> <img src="<?php echo drupal_get_path('theme', 'psb_tb3'); ?>/images/lion.png" alt="Penn State Lion" class="hidden-sm hidden-xs Lion" style="margin-top:5px;width:100%;"/></a>
            <ul
            class="nav nav-tabs nav-stacked" id="frontnav" style="margin-top:">

                <li style="border-top: 2px solid #ffffff;"><a href="future-students">Future Students</a>
                </li>
                <li style="border-top: 2px solid #ffffff;"><a href="current-students">Current Students</a>
                </li>
                <li style="border-top: 2px solid #ffffff;"><a href="alumni">Alumni &amp; Friends</a>
                </li>
                <li style="border-top: 2px solid #ffffff;"><a href="faculty-staff">Faculty &amp; Staff</a>
                </li>  <img style="margin-top:5px;" width="100%" src="<?php echo drupal_get_path('theme', 'psb_tb3'); ?>/images/penn_state_lives_here.jpg" alt="Penn State Lion" class="hidden-sm hidden-xs Lion" style="width:100%;height:auto;"/></a>

                                <li>
                </li>
                </ul>
                <!--<div class="col-md-3" style="margin-left: 0px !important; background-color: #0066CC !important; padding-top: 5px !important; padding-bottom: 5px !important;"> <a href="https://www.facebook.com/pennstatebehrend"><img src="/sites/all/themes/psb/images/facebook_32.png" style="margin-left: 1em;" alt="Link to Penn State Behrend's Facebook Page" class="img-responsive" /></a>
                    <a
                    href="https://twitter.com/psbehrend">
                        <img src="/sites/all/themes/psb/images/twitter_32.png" style="margin-left: 1em;"
                        alt="Link to Penn State Behrend's Twitter Page" class="img-responsive"
                        />
                        </a> <a href="//www.youtube.com/PennStateBehrend1"><img src="/sites/all/themes/psb/images/youtube_32.png" style="margin-left: 1em;" alt="Link to Penn State Behrend's YouTube Channel" class="img-responsive" /></a>
                        <a
                        href="//instagram.com/psbehrend">
                            <img src="/sites/all/themes/psb/images/instagram.png" style="margin-left: 1em;"
                            alt="View Penn State Behrend on Instagram" class="img-responsive" />
                            </a>
                </div>-->
        </div>
    </div>
</div>
        <div class="container" style="padding-left: 0 !important;padding-right: 0 !important;margin-bottom:10px;">
            <div id="news" class="news col-md-5" style="margin-left: 0 !important; padding:0px;"><?php print render($page['news']); ?></div>

            <div id="events" class="events col-md-5" style=""><?php print render($page['events']); ?></div>
            <div class="buttons col-xs-3 col-md-2 visible-md visible-lg" style="margin-left: 0 !important; padding:0px;margin-right:0 !important;"><?php print render($page['buttons']); ?></div>

        </div>
       <!-- <div class="row visible-md">
            <div class="col-md-12 btn-group" style="text-align: center;"> <a class="btn btn-primary" href="//behrend.psu.edu/Events/MasterCalendar.aspx"
                target="_blank">Events Calendar</a>  <a class="btn btn-primary" href="//behrend.psu.edu/VirtualTour/PSBVirtualTour.html"
                target="_blank" title="Take the Penn State Behrend Virtual Tour">Virtual Tour</a>
                <a
                href="//behrendblog.wordpress.com/ " target="_blank" class="btn btn-primary"
                title="Read the Penn State Behrend Blog">Blog</a> <a class="btn btn-primary" href="/webcams" target="_blank" title="View the Penn State Behrend Web Cams">Webcams</a>
                    <a
                    href="/admissions-financial-aid/visit-behrend" class="btn btn-primary"
                    title="Visit Us">Visit</a> <a href="/admissions-financial-aid/apply" class="btn btn-primary">Apply</a>
            </div>
        </div>
        <div class="row visible-sm">
            <div class="col-md-6 btn-group" style="text-align: center;"> <a class="btn btn-primary" href="//behrend.psu.edu/Events/MasterCalendar.aspx"
                target="_blank">Events Calendar</a>  <a href="//behrendblog.wordpress.com/ "
                target="_blank" class="btn btn-primary" title="Read the Penn State Behrend Blog">Blog</a>
                <a
                class="btn btn-primary" href="/webcams" target="_blank" title="View the Penn State Behrend Web Cams">Webcams</a> <a href="/admissions-financial-aid/visit-behrend" class="btn btn-primary"
                    title="Visit Us">Visit</a>
 <a href="/admissions-financial-aid/apply" class="btn btn-primary">Apply</a>
            </div>
        </div> -->
 <footer class="footer container"></footer>
    </div>
</div>
<div role="contentinfo" class="clearfix footer" id="footer" style="background-color:#262c3a;">
  <div class="container" style="padding-left:0px;">
    <div class="row" id="footer-content">
      <div class="col-xs-12 col-sm-1-4 col-md-2">
        <div class="block"><h3><a href="//psbehrend.psu.edu/Academics" alt="Academics">Academics</a></h3>
          <ul class="footer-list">
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/school-of-business">Black School of Business</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/school-of-engineering">School of Engineering</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/school-of-humanities-social-sciences">School of Humanities & Social Sciences</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/school-of-science">School of Science</a></li>            </ul>

            <ul class="footer-list">
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a href="//psbehrend.psu.edu/Academics/academic-programs/majors-minors" alt="Undergraduate Majors & Minors">Undergraduate Majors & Minors</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/admissions-financial-aid/graduate-admissions">Graduate Programs</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/Academics/academic-services/acpc">Academic & Career Planning</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" target="_blank" href="http://psuerie.bncollege.com/">Bookstore</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/Academics/academic-services/college-registrar" title="College Registrar">College Registrar</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" target="_blank" href="//psbehrend.psu.edu/Academics/academic-services/lrc">Learning Resource Center</a></li>
                        <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" target="_blank" href="http://www.libraries.psu.edu/psul/erie.html">Lilley Library</a></li>
          </ul>

        </div>
      </div>
      <div class="col-xs-12 col-sm-1-4 col-md-2">
        <div class="block">
          <h3><a href="//psbehrend.psu.edu/admissions-financial-aid" alt="Admissions">Admission</a></h3>
          <ul class="footer-list">
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/admissions-financial-aid/undergraduate-admissions">Undergraduate</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/admissions-financial-aid/graduate-admissions">Graduate</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/admissions-financial-aid/adult-admissions">Adult Learners</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/admissions-financial-aid/financial-aid/types-of-aid/veterans-benefits-1">Veterans</a></li></ul>
            <h3><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/admissions-financial-aid/financial-aid">Tuition & Financial Aid</a></h3>
            <ul class="footer-list">

            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/admissions-financial-aid/financial-aid/tuition-and-costs">Tuition & Costs</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/admissions-financial-aid/financial-aid/applying-for-aid">Applying for Aid</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/admissions-financial-aid/financial-aid/types-of-aid/online-resources">Financial Aid Resources</a></li>
            </ul>
        </div>
      </div>
      <div class="col-xs-12 col-sm-1-4 col-md-2">
        <div class="block">
          <h3><a href="//psbehrend.psu.edu/student-life" alt="Student Life">Student Life</a></h3>
          <ul class="footer-list">
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/student-life/housing-and-residence-life">Housing & Residence Life</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/student-life/student-services">Student Services</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/student-life/educational-equity-and-diversity">Educational Equity & Diversity</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/student-life/student-activities-1">Student Activities</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="http://www.psblions.com/landing/index" target="_blank">Athletics</a></li>
            </ul>
          <h3><a href="//psbehrend.psu.edu/admissions-financial-aid/visit-behrend">Visiting Behrend</a></h3>
          <ul class="footer-list">
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/sites/default/files/admissions-financial-aid/visit-behrend/CampusMap.pdf">Campus Map</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/about-the-college/about-erie/getting-here-directions">Directions</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/student-life/student-services/police/parking">Parking</a></li>

          </ul>                    <h3><a href="//psbehrend.psu.edu/about-the-college/people-and-departments">People & Departments<a></h3>
        </div>
      </div>
      <div class="col-xs-12 col-sm-1-4 col-md-2">
        <div class="block">
          <h3><a href="//psbehrend.psu.edu/research-outreach">Research</a></h3>
          <ul class="footer-list">
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/research-outreach/student-research">Student Research</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link"href="//psbehrend.psu.edu/research-outreach/faculty-research">Faculty Research</a></li>
          </ul>
          <h3><a href="//psbehrend.psu.edu/research-outreach">Outreach</a></h3>
          <ul class="footer-list">
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/research-outreach/outreach-initiatives">Outreach Centers & Initiatives</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" target="_blank" href="http://knowledgepark.psu.edu/">Knowledge Park</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/community-and-workforce-programs">Community & Workforce Programs</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" target="_blank" href="http://www.paseagrant.org/">Pennsylvania Sea Grant</a></li>
            <li><span style="color:#FFF;font-size:12px;">&gt; </span><a data-ua-label="footer" class="su-link" href="//psbehrend.psu.edu/school-of-science/programs-events-1/programs-events/yahn-planetarium-at-penn-state-behrend">Yahn Planetarium</a></li>
          </ul>
          <h3><a href="//psbehrend.psu.edu/alumni/giving-to-behrend">Giving to Behrend</a></h3>

        </div>
        <div class="block">

        </div>
      </div>
      <div class="col-xs-12 col-sm-1-4 col-md-2">

      </div>
      <div class="col-xs-12 col-md-2" id="hp-buttons" style="padding-right:0px;">
        <ul>
                    <li><a data-ua-action="visitors-gateway" data-ua-label="footer" class="btn btn-primary btn-u btn-block su-link" href="//psbehrend.psu.edu/admissions-financial-aid/visit-behrend">Visit Behrend</a> </li>
          <li><a class="btn btn-primary btn-u btn-block" href="//psbehrend.psu.edu/admissions-financial-aid/apply">Apply to Behrend</a> </li>

          <li><a data-ua-label="footer" class="btn btn-primary btn-u btn-block su-link" href="//psbehrend.psu.edu/Academics/admissions-financial-aid/visit-behrend/copy_of_getting-here">Getting Here</a> </li>
          <li><a data-ua-label="footer" class="btn btn-primary btn-u btn-block su-link" href="//psbehrend.psu.edu/news-events">News & Events</a> </li>
          <li><a data-ua-label="footer" class="btn btn-primary btn-u btn-block su-link" href="//behrendblog.wordpress.com/">Blog</a> </li>
          <!--<li><a class="btn btn-primary btn-u btn-block" href="//psbehrend.psu.edu/webcam">Webcams</a> </li> -->
        </ul>
      </div>
    </div>
  </div>

    <!-- Global footer snippet start -->  <div id="print-footer" class="visible-print-block"><div class="row">
    <address>Penn State Erie, The Behrend College | 4701 College Drive, Erie, PA 16563 | 814-898-6000 | Toll-free: 866-374-3378</address></div>
    </div>
 <div id="global-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-7 pull-left" id="bottom-logo"> <p>Penn State Erie, The Behrend College | 4701 College Drive, Erie, PA 16563 | 814-898-6000 | Toll-free: 866-374-3378 <br /><p style="margin-top:5px;"><a href="https://webforms.bd.psu.edu/registration/report-web-site-problem" alt="Report a website problem" target="_blank">Website Feedback</a> | <a href="//psbehrend.psu.edu/sitemap" alt="View the Sitemap">Site Map</a> | <a href="/login" alt="Login">Login</a></p></div>
        <!-- #bottom-logo end -->
        <div class="col-md-5 pull-right" id="bottom-text">
          <ul>
            <li class="maps alt"><a data-ua-label="global-footer" data-ua-action="//psbehrend.psu.edu/accessibility" class="su-link" href="//psbehrend.psu.edu/accessibility">Accessibility</a></li>
            <li class="search-stanford"><a href="//www.psu.edu/copyright-information">Copyright</a></li>
            <li class="terms alt"><a href="//www.psu.edu/legal-statements">Privacy &amp Legal Statements</a></li>
            <li class="emergency-info"><a data-ua-label="global-footer" class="su-link" href="//www.psu.edu/hotlines">Hotlines</a></li>
          </ul>
         <p class="pull-right">The Pennsylvania State University © 2016</p>
        </div>
        <!-- .bottom-text end -->
        <div class="clear"></div>
        </div>
      <!-- .row end -->
    </div>
    <!-- .container end -->
  </div>
  <!-- global-footer end -->
  <!-- Global footer snippet end -->

</div>
