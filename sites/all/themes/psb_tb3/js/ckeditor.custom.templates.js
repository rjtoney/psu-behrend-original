// Register a template definition set named "default".
CKEDITOR.addTemplates( 'default',
{
  // The name of the subfolder that contains the preview images of the templates.
  imagesPath : CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),

  // Template definitions.
  templates :
    [
      {
        title: 'Single Column Template',
        image: 'singlecolumn.gif',
        description: 'Single Column Template.  To be used in the event you wish to change a 2 or 3 column template to a single template',
        html:
          '<div class="width:100%;">' +
          '<p>Type your text here.</p>' +
          '</div>'
      },
      {
        title: 'Two Column Layout (Text Only)',
        image: 'twocolumn_textonly.gif',
        description: 'Two Column Text Only Template.',
        html:
          '<div style="width:50%;float:left;">' +
          '<p>Column 1</p>' +
          '<br /></div>' +
          '<div style="width:50%;float:right;">' +
          '<p>Column 2</p>' +
          '<br /></div>'
      },
       {
        title: 'Two Column Layout Image on Right',
        image: '2columns_imageright.gif',
        description: 'Two Column Template with Image on the right.',
        html:
          '<div style="width:50%;float:left;">' +
          '<p>YOUR TEXT GOES HERE </p><br /></div>' +
          '<div style="width:50%;float:right;"><img alt="64x64" data-src="holder.js/64x64" class="media-object" style="width: 250px; height: 250px;" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI2NCIgaGVpZ2h0PSI2NCI+PHJlY3Qgd2lkdGg9IjY0IiBoZWlnaHQ9IjY0IiBmaWxsPSIjZWVlIi8+PHRleHQgdGV4dC1hbmNob3I9Im1pZGRsZSIgeD0iMzIiIHk9IjMyIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+NjR4NjQ8L3RleHQ+PC9zdmc+">' +
          '</div>'
      },


       {
        title: 'Two Column Layout Image on Left',
        image: '2columns_imageleft.gif',
        description: 'Two Column Template with Image on the left.',
        html:
          '<div style="width:50%;float:left;"><img alt="64x64" data-src="holder.js/64x64" class="media-object" style="width: 250px; height: 250px;" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI2NCIgaGVpZ2h0PSI2NCI+PHJlY3Qgd2lkdGg9IjY0IiBoZWlnaHQ9IjY0IiBmaWxsPSIjZWVlIi8+PHRleHQgdGV4dC1hbmNob3I9Im1pZGRsZSIgeD0iMzIiIHk9IjMyIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+NjR4NjQ8L3RleHQ+PC9zdmc+">' +
          '</div>' +
          '<div style="width:50%;float:right;">' +
          '<p>YOUR TEXT GOES HERE </p><br /></div>'
      }
    ]
});
