(function ($) {

Drupal.behaviors.jquerymenu = {
    attach:function(context) {


  $('ul.jquerymenu .open').parents('li').removeClass('closed').addClass('open');
  $('ul.jquerymenu .open').parents('li').children('span.parent').removeClass('closed').addClass('open').slideDown('700');
  $('ul.jquerymenu .active').parents('li').removeClass('closed').addClass('open');
  $('ul.jquerymenu .active').parents('li').children('span.parent').removeClass('closed').addClass('open');


  // create functions for hover effects
  jqm_showit = function() {
    $(this).children('.jqm_link_edit').fadeIn();
  }
  jqm_hideit = function() {
    $(this).children('.jqm_link_edit').fadeOut();
  }
  $('ul.jquerymenu li').hover(jqm_showit, jqm_hideit);

  jqm_mouseenter = function() {
    momma = $(this);
    if ($(momma).hasClass('closed')){
      if (Drupal.settings.jquerymenu.animate === 1) {
        $($(this).siblings('ul').children()).hide().fadeIn('3000');
        $(momma).children('ul').slideDown('700');
      }
      $(momma).removeClass('closed').addClass('open');
      $(this).removeClass('closed').addClass('open');
    }
  }

  jqm_mouseleave = function(){
    momma = $(this);
    if ($(momma).hasClass('open')){
      if (Drupal.settings.jquerymenu.animate === 1) {
        $(momma).children('ul').slideUp('700');
        $($(this).siblings('ul').children()).fadeOut('3000');
      }
      $(momma).removeClass('open').addClass('closed');
      $(this).removeClass('open').addClass('closed');
    }
  }

  // set selector variable
  var selector = "li.parent span.parent";
  if (Drupal.settings.jquerymenu.click_to_expand) {
    selector = selector + ', li.parent > a';
  }

  // if hover, change menu state
  if (Drupal.settings.jquerymenu.hover === 1) {
    $('ul.jquerymenu:not(.jquerymenu-processed)', context).addClass('jquerymenu-processed').each(function(){
      $(this).find(selector).hover(jqm_mouseenter, jqm_mouseleave);
    });
    // remove the + and - boxes
    $('ul.jquerymenu-processed span.parent').remove();
    return false;
  }

  // if not hover, change menu state on click
  else if (Drupal.settings.jquerymenu.hover === 0) {
    $('ul.jquerymenu:not(.jquerymenu-processed)', context).addClass('jquerymenu-processed').each(function(){
      $(this).find(selector).click(function(){
        momma = $(this).parent();
        if ($(momma).hasClass('closed')){
          if (Drupal.settings.jquerymenu.animate === 1) {
            $($(this).siblings('ul').children()).hide().fadeIn('3000');
            $(momma).children('ul').slideDown('700');
          }
          $(momma).removeClass('closed').addClass('open');
          $(this).removeClass('closed').addClass('open');
          return false;
        }
        else{
          if (Drupal.settings.jquerymenu.animate === 1) {
            $(momma).children('ul').slideUp('700');
            $($(this).siblings('ul').children()).fadeOut('3000');
          }
          $(momma).removeClass('open').addClass('closed');
          $(this).removeClass('open').addClass('closed');
          return false;
        }
      });
    });
  }
}
}
})(jQuery);
;
// Wrap in onReady function using jQuery. Don't assume jQuery is located at $.
jQuery( document ).ready(function( $ ) {
  				/** the element*/
				var $ui = $('#ui_element');
				/**on focus and on click display the dropdown*/
				$ui.find('.sb_input').bind('focus click', function($) {
					$ui.find('.sb_dropdown').show();
				});
				/**on mouse leave hide the dropdown*/
				$ui.bind('mouseleave', function($) {
					$ui.find('.sb_dropdown').hide();
				});
				
				/* Hide engines that were not enabled via the settings in the Drupal 
				 * block configure.
				 */
				var psu_show = Drupal.settings.psu_drupal_search_block.show;
				for (engine in psu_show) {
					if (psu_show[engine]==0)
						$('#engine-'+engine).hide();
				}
				
				// Select default search engine.
				$('input#'+Drupal.settings.psu_drupal_search_block.default_engine).attr("checked","checked");
				
				//Set Search box placeholder text.
				$('#ui_element input#search').attr('placeholder', Drupal.settings.psu_drupal_search_block.placeholder);
				
});

function multiPSUsearch() {
	with (window.document) {

		var elements = getElementsByName('engine');
		for (var k = 0; k < elements.length; k++)
			if (elements[k].checked) {
				var c = parseInt(elements[k].value);
			}
			
		/* Send value to sub-form to query on specific search engine.
		 * 
		 * Value is copied from the main search form.
		 * Based on the selection from the radio-buttons, it is pasted
		 * into the corresponding sub-form for that search engine.
		 * That form is then submitted resulting in the search executing
		 * on the correctly selected search engine.
		 */
		var v = searchengine.search.value;
		switch (c) {
			case 1:
				psupeople.cn.value = v;
				psupeople.action = "http://www.psu.edu/cgi-bin/ldap/ldap_query.cgi";
				psupeople.submit();
				break;
			case 2:
				psudepts.dept_name.value = v;
				psudepts.action = "http://www.psu.edu/cgi-bin/ldap/dept_query.cgi";
				psudepts.submit();
				break;
			case 3:
				thissite.keys.value = v;
				thissite.action = Drupal.settings.psu_drupal_search_block.this_site_url;
				thissite.submit();
				break;
			default:
				window.open("http://www.psu.edu/search/gss/" + v, "_self")
		}
	}
}
;
(function ($) {
  Drupal.quickbar = Drupal.quickbar || {};
  
  Drupal.quickbar.setActive = function(toolbar_id) {
    // Show the right toolbar
    $('#quickbar .depth-1 ul.links').addClass('collapsed');
    $(toolbar_id).removeClass('collapsed');
    $('div#quickbar, div#quickbar .depth-1').removeClass('collapsed');
  
    // Switch link active class to corresponding menu item
    var link_id = toolbar_id.replace('quickbar', 'quickbar-link');
    $('#quickbar .depth-0 ul.links a').removeClass('active');
    $(link_id).addClass('active');

    $('#quickbar').addClass('quickbar-open');
  }
  
  Drupal.behaviors.quickbar = {
    attach: function (context, settings) {
      // Move the toolbar to underneath body.
      // TODO: I'm not sure that these lines are necessary anymore
      // since we put this in page_top in D7?!
      var toolbarHtml = $('#quickbar').remove();
      $('body').prepend(toolbarHtml);
      
      // Primary menus
      $('#quickbar .depth-0 ul.links a:not(.processed)').each(function() {
        var target = $(this).attr('id');
        if (target) {
          // Build a new id to check for a corresponding secondary menu...
          target = '#'+ target.replace('quickbar-link', 'quickbar');
          if ($(target, '#quickbar').size() > 0) {
            // If secondary menu should show on page load AND
            // if this link (in .depth-0) is active...show this toolbar on setup
            if (Drupal.settings.quickbar.secondary_menu_visibility && $(this).parent().is('.active-trail')) {
              Drupal.quickbar.setActive(target);
            }
            // Add click handler
            $(this).click(function() {
              if ($(this).is('.active')) {
                // Follow the link
                if (!$('#quickbar').hasClass('primary-nofollow')) {
                  window.location = $(this).attr('href');
                  return true;
                }
                return false;
              }
              // Open submenu
              Drupal.quickbar.setActive(target);
              return false;
            });
          }
        }
        $(this).addClass('processed');
      });
      
      // Close button
      $('#quickbar .depth-1 span.close:not(.processed)').each(function() {
        $(this).click(function() {
          $('#quickbar .depth-1').addClass('collapsed');
          $('#quickbar-admin a.active').removeClass('active');
          $('#quickbar').removeClass('quickbar-open');
          return false;
        });
        $(this).addClass('processed');
      });
      
      // Secondary menus
      var secondary = $('#quickbar .depth-1');
      // If we want to hide the secondary menu on page load them we need
      // to add the collapse class to the secondary wrapper.
      if (!Drupal.settings.quickbar.secondary_menu_visibility) {
        secondary.addClass('collapsed');
      }
      
      secondary.find('ul.links:not(.processed)').each(function() {
        $(this).addClass('processed');
      });
    }
  };
})(jQuery);;
(function ($) {

/**
 * Show/hide the 'Email site administrator when updates are available' checkbox
 * on the install page.
 */
Drupal.hideEmailAdministratorCheckbox = function () {
  // Make sure the secondary box is shown / hidden as necessary on page load.
  if ($('#edit-update-status-module-1').is(':checked')) {
    $('.form-item-update-status-module-2').show();
  }
  else {
    $('.form-item-update-status-module-2').hide();
  }

  // Toggle the display as necessary when the checkbox is clicked.
  $('#edit-update-status-module-1').change( function () {
    $('.form-item-update-status-module-2').toggle();
  });
};

/**
 * Internal function to check using Ajax if clean URLs can be enabled on the
 * settings page.
 *
 * This function is not used to verify whether or not clean URLs
 * are currently enabled.
 */
Drupal.behaviors.cleanURLsSettingsCheck = {
  attach: function (context, settings) {
    // This behavior attaches by ID, so is only valid once on a page.
    // Also skip if we are on an install page, as Drupal.cleanURLsInstallCheck will handle
    // the processing.
    if (!($('#edit-clean-url').length) || $('#edit-clean-url.install').once('clean-url').length) {
      return;
    }
    var url = settings.basePath + 'admin/config/search/clean-urls/check';
    $.ajax({
      url: location.protocol + '//' + location.host + url,
      dataType: 'json',
      success: function () {
        // Check was successful. Redirect using a "clean URL". This will force the form that allows enabling clean URLs.
        location = settings.basePath +"admin/config/search/clean-urls";
      }
    });
  }
};

/**
 * Internal function to check using Ajax if clean URLs can be enabled on the
 * install page.
 *
 * This function is not used to verify whether or not clean URLs
 * are currently enabled.
 */
Drupal.cleanURLsInstallCheck = function () {
  var url = location.protocol + '//' + location.host + Drupal.settings.basePath + 'admin/config/search/clean-urls/check';
  // Submit a synchronous request to avoid database errors associated with
  // concurrent requests during install.
  $.ajax({
    async: false,
    url: url,
    dataType: 'json',
    success: function () {
      // Check was successful.
      $('#edit-clean-url').attr('value', 1);
    }
  });
};

/**
 * When a field is filled out, apply its value to other fields that will likely
 * use the same value. In the installer this is used to populate the
 * administrator e-mail address with the same value as the site e-mail address.
 */
Drupal.behaviors.copyFieldValue = {
  attach: function (context, settings) {
    for (var sourceId in settings.copyFieldValue) {
      $('#' + sourceId, context).once('copy-field-values').bind('blur', function () {
        // Get the list of target fields.
        var targetIds = settings.copyFieldValue[sourceId];
        // Add the behavior to update target fields on blur of the primary field.
        for (var delta in targetIds) {
          var targetField = $('#' + targetIds[delta]);
          if (targetField.val() == '') {
            targetField.val(this.value);
          }
        }
      });
    }
  }
};

/**
 * Show/hide custom format sections on the regional settings page.
 */
Drupal.behaviors.dateTime = {
  attach: function (context, settings) {
    for (var fieldName in settings.dateTime) {
      if (settings.dateTime.hasOwnProperty(fieldName)) {
        (function (fieldSettings, fieldName) {
          var source = '#edit-' + fieldName;
          var suffix = source + '-suffix';

          // Attach keyup handler to custom format inputs.
          $('input' + source, context).once('date-time').keyup(function () {
            var input = $(this);
            var url = fieldSettings.lookup + (/\?/.test(fieldSettings.lookup) ? '&format=' : '?format=') + encodeURIComponent(input.val());
            $.getJSON(url, function (data) {
              $(suffix).empty().append(' ' + fieldSettings.text + ': <em>' + data + '</em>');
            });
          });
        })(settings.dateTime[fieldName], fieldName);
      }
    }
  }
};

 /**
 * Show/hide settings for page caching depending on whether page caching is
 * enabled or not.
 */
Drupal.behaviors.pageCache = {
  attach: function (context, settings) {
    $('#edit-cache-0', context).change(function () {
      $('#page-compression-wrapper').hide();
      $('#cache-error').hide();
    });
    $('#edit-cache-1', context).change(function () {
      $('#page-compression-wrapper').show();
      $('#cache-error').hide();
    });
    $('#edit-cache-2', context).change(function () {
      $('#page-compression-wrapper').show();
      $('#cache-error').show();
    });
  }
};

})(jQuery);
;
